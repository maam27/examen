﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for PermissionSystem.xaml
    /// </summary>
    public partial class PermissionSystem : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        public PermissionSystem()
        {
            InitializeComponent();
            RetrieveData();
        }

        public void RetrieveData()
        {
            var test = from perm in zs.zs_groepen select perm;
            PermissionGrid.ItemsSource = test.ToList();

            //set datagrid colors
            this.PermissionGrid.RowBackground = Color.GridMain();
            this.PermissionGrid.AlternatingRowBackground = Color.GridSecond();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            zs.SaveChanges();
            RetrieveData();
        }

        private void NieuwGroep(object sender, RoutedEventArgs e)
        {
            TextPopup txt = new TextPopup();
            txt.Owner = this;
            txt.Title = "Groep naam";
            if (txt.ShowDialog() == true)
            {
                zs_groepen groep = new zs_groepen();
                groep.naam = txt.returntext;
                zs.zs_groepen.Add(groep);
                zs.SaveChanges();
            }
            RetrieveData();
        }

        private void VerwijderGroep(object sender, RoutedEventArgs e)
        {
            if (PermissionGrid.SelectedIndex != -1)
            {
                try
                {
                    object item = PermissionGrid.SelectedItem;
                    int ID = Convert.ToInt32((PermissionGrid.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text);
                    zs_groepen groep = zs.zs_groepen.FirstOrDefault(g => g.id == ID);

                    MessageBoxResult result = System.Windows.MessageBox.Show(String.Format("Weet u zeker dat u de gebruikers groep {0} wil verwijderen?",groep.naam), "Verwijder Groep?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.No)
                    { return; }

                    if (groep.zs_users.Count() <= 0)
                    {
                        zs.zs_groepen.Remove(groep);
                        zs.SaveChanges();
                        RetrieveData();
                    }
                    else
                    {
                        FailedRemove();
                    }
                }
                catch (Exception error) { FailedRemove(); }
            }
        }

        private void FailedRemove()
        {
            System.Windows.Forms.MessageBox.Show("Er is iets misgegaan bij het verwijderen" + Environment.NewLine + "Zorg er voor dat u zeker weet dat deze groep niet in gebruik is.");
        }
    }
}
