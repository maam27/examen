﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Overzicht.xaml
    /// </summary>
    public partial class Overzicht : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        public string huidigoverzicht { get; set; }
        public Overzicht()
        {
            InitializeComponent();
            
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
            FillGridAll();
        }
        //queries
        public void FillGridAll()
        {
            huidigoverzicht = "Alle cursisten";
            var test =
                zs.zs_users.ToList().Select(i => new
                {
                    Id = i.id,
                    Gebruiker = i.voornaam + " " + i.tussenvoegsel + " " + i.achternaam,
                    Email = i.email,
                    Geslacht = i.geslacht,
                    GeboorteDatum = i.geboortedatum,
                    Plaats = i.plaats,
                    Straat = i.Vstraat,
                    Postcode = i.Fpostcode,
                    TelefoonNummer = i.telnummer,
                    MobielNummer = i.mobnummer
                }
                );

            try
            {
                OverzichtGrid.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }

        }

        private void MedewerkerRooster()
        {           
            zs_users user = zs.zs_users.FirstOrDefault(u => u.id == CurrentUser.Current.id);

            var rooster = zs.zs_geplandeinstructeurs.Where(i => i.user_id == user.id).Select(i => new
            {
                ID = i.id,
                Cursus = i.zs_cursusregel.zs_cursus.naam,
                CursusBeginTijd = i.zs_cursusregel.begintijdendatum,
                CursusEindTijd = i.zs_cursusregel.eindtijdendatum
            })
            .OrderBy(o => o.CursusBeginTijd)
            .ThenBy(o => o.CursusEindTijd)
            .ThenBy(o => o.Cursus);

            try
            {
                this.OverzichtGrid.ItemsSource = rooster.ToList();
            }
            catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }
        }

        public void FillGridJaar()
        {
            var test =
            zs.zs_cursusregel.ToList().Where(d => d.begintijdendatum.Year == DateTime.Now.Year ||
                d.eindtijdendatum.Year == DateTime.Now.Year
                ).Select(i => new
                {
                    Id = i.id,
                    Naam = i.zs_cursus.naam,
                    Plaatsen = i.zs_cursus.aantalplaatsenbeschikbaar,
                    Beschrijving = i.zs_cursus.beschrijving,
                    PlaatsenBezet = zs.zs_inschrijvingen.Where(ii => ii.status == "Geaccepteerd" && ii.betaald == 1 && ii.zs_cursusregel.id == i.id).GroupBy(ii => ii.zs_cursusregel.id).Count(),
                    Prijs = i.zs_cursus.prijs,
                    Totaal = zs.zs_inschrijvingen.Where(ii => ii.status == "Geaccepteerd" && ii.betaald == 1 && ii.zs_cursusregel.id == i.id).GroupBy(ii => ii.zs_cursusregel.id).Count()*i.zs_cursus.prijs      //totale prijs
                }
                );
           

            try
            {
                OverzichtGrid.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }

        }

        public void FillGridInschrijving()
        {
            var test =
                zs.zs_inschrijvingen.ToList().Select(i => new
                {
                    Id = i.id,
                    Gebruiker = i.zs_users.voornaam + " " + i.zs_users.tussenvoegsel + " " + i.zs_users.achternaam,
                    Cursus = i.zs_cursusregel.zs_cursus.naam,
                    Status = i.status,
                    Datum = i.zs_cursusregel.begintijdendatum,
                    EindDatum = i.zs_cursusregel.eindtijdendatum,
                    DatumAangevraagd = i.datumaangevraagd
                }
                );

            try
            {
                OverzichtGrid.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }

        }

        public void FillGridNCursisten()
        {
            var test =
            zs.zs_users.ToList().Where(a => a.DatumAangemaakt <= DateTime.Now && a.DatumAangemaakt >= DateTime.Now.AddMonths(-1)).Select(i => new
                {
                    Id = i.id,
                    Gebruiker = i.voornaam + " " + i.tussenvoegsel + " " + i.achternaam,
                    Email = i.email,
                    Geslacht = i.geslacht,
                    GeboorteDatum = i.geboortedatum,
                    Plaats = i.plaats,
                    Straat = i.Vstraat,
                    Postcode = i.Fpostcode,
                    TelefoonNummer = i.telnummer,
                    MobielNummer = i.mobnummer
                }
                );

            try
            {
                OverzichtGrid.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }

        }
        //namen van buttons
        private void Rooster(object sender, RoutedEventArgs e)
        {
            huidigoverzicht = "Rooster medewerkers";
            MedewerkerRooster();
        }

        private void Allemaal(object sender, RoutedEventArgs e)
        {
            huidigoverzicht = "Alle cursisten";
            FillGridAll();
        }

        private void Jaar(object sender, RoutedEventArgs e)
        {
            huidigoverzicht = "Bezetting afgelopen jaar";
            FillGridJaar();
        }

        private void Inschrijvingen(object sender, RoutedEventArgs e)
        {
            huidigoverzicht = "Inschrijvingen";
            FillGridInschrijving();
        }

        private void NieuweCursisten(object sender, RoutedEventArgs e)
        {
            huidigoverzicht = "Nieuwe Cursisten";
            FillGridNCursisten();
        }
        //excel gedeelte
        private void MedewerkerRoosterExcel()
        {
            if (CurrentUser.Current != null)
            {
                string savelocation = Properties.Settings.Default.SavePath;
                string FileName = "Rooster - "
                + CurrentUser.Current.voornaam + " " + CurrentUser.Current.tussenvoegsel + " " + CurrentUser.Current.achternaam
                + " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                try
                {
                    //Start Excel and get Application object.
                    oXL = new Microsoft.Office.Interop.Excel.Application();
                    oXL.Visible = true;

                    //Get a new workbook.
                    oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                    oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                    //verkrijg gegevens
                    zs_users user = zs.zs_users.FirstOrDefault(u => u.id == CurrentUser.Current.id);
                    var rooster = zs.zs_geplandeinstructeurs.Where(i => i.user_id == user.id).Select(i => new
                    {
                        ID = i.id,
                        Cursus = i.zs_cursusregel.zs_cursus.naam,
                        CursusBeginTijd = i.zs_cursusregel.begintijdendatum,
                        CursusEindTijd = i.zs_cursusregel.eindtijdendatum
                    })
                    .OrderBy(o => o.CursusBeginTijd)
                    .ThenBy(o => o.CursusEindTijd)
                    .ThenBy(o => o.Cursus).ToList();

                    //zet headers
                    int currentrow = 1;
                    oSheet.Cells[currentrow, 1] = "Cursus Naam";
                    oSheet.Cells[currentrow, 2] = "Begin datum";
                    oSheet.Cells[currentrow, 3] = "Begin tijd";
                    oSheet.Cells[currentrow, 5] = "Eind datum";
                    oSheet.Cells[currentrow, 6] = "Eind tijd";
                    //zet headers bold
                    oSheet.get_Range("A" + currentrow.ToString(), "F" + currentrow.ToString()).Font.Bold = true;
                    currentrow++;
                    //zet gegevens
                    foreach (var cursus in rooster)
                    {
                        oSheet.Cells[currentrow, 1] = cursus.Cursus;
                        oSheet.Cells[currentrow, 2] = String.Format("{0:dd-MM-yyyy}", cursus.CursusBeginTijd);
                        oSheet.Cells[currentrow, 3] = String.Format("{0:HH:mm}", cursus.CursusBeginTijd);
                        oSheet.Cells[currentrow, 4] = "tot";
                        oSheet.Cells[currentrow, 5] = String.Format("{0:dd-MM-yyyy}", cursus.CursusEindTijd);
                        oSheet.Cells[currentrow, 6] = String.Format("{0:HH:mm}", cursus.CursusEindTijd);
                        currentrow++;
                    }

                    //AutoFit columns
                    oRng = oSheet.get_Range("A1", "K1");
                    oRng.EntireColumn.AutoFit();

                    oXL.Visible = false;
                    oXL.UserControl = false;
                    oWB.SaveAs(savelocation + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                    false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    oWB.Close();                  
                }
                catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }
            }
        }

        private void AlleCursistenExcel()
        {
            
            
                string savelocation = Properties.Settings.Default.SavePath;
                string FileName = "Alle Cursisten - "
                +  " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                try
                {
                    //Start Excel and get Application object.
                    oXL = new Microsoft.Office.Interop.Excel.Application();
                    oXL.Visible = true;

                    //Get a new workbook.
                    oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                    oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                    //verkrijg gegevens
                    var test =
                zs.zs_users.ToList().Select(i => new
                {
                    Id = i.id,
                    Gebruiker = i.voornaam + " " + i.tussenvoegsel + " " + i.achternaam,
                    Email = i.email,
                    Geslacht = i.geslacht,
                    GeboorteDatum = i.geboortedatum,
                    Plaats = i.plaats,
                    Straat = i.Vstraat,
                    Postcode = i.Fpostcode,
                    TelefoonNummer = i.telnummer,
                    MobielNummer = i.mobnummer
                }
                ).ToList();

                    //zet headers
                    int currentrow = 1;
                    oSheet.Cells[currentrow, 1] = "Naam";
                    oSheet.Cells[currentrow, 2] = "Email";
                    oSheet.Cells[currentrow, 3] = "Geslacht";
                    oSheet.Cells[currentrow, 4] = "Geboortedatum";
                    oSheet.Cells[currentrow, 5] = "Plaats";
                    oSheet.Cells[currentrow, 6] = "Straat";
                    oSheet.Cells[currentrow, 7] = "Postcode";
                    oSheet.Cells[currentrow, 8] = "Telefoon nummer";
                    oSheet.Cells[currentrow, 9] = "Mobiele nummer";
                    //zet headers bold
                    oSheet.get_Range("A" + currentrow.ToString(), "Z" + currentrow.ToString()).Font.Bold = true;
                    currentrow++;
                    //zet gegevens
                    foreach (var cursist in test)
                    {
                        oSheet.Cells[currentrow, 1] = cursist.Gebruiker;
                        oSheet.Cells[currentrow, 2] = cursist.Email;
                        oSheet.Cells[currentrow, 3] = cursist.Geslacht;
                        oSheet.Cells[currentrow, 4] = cursist.GeboorteDatum;
                        oSheet.Cells[currentrow, 5] = cursist.Plaats;
                        oSheet.Cells[currentrow, 6] = cursist.Straat;
                        oSheet.Cells[currentrow, 7] = cursist.Postcode;
                        oSheet.Cells[currentrow, 8] = cursist.TelefoonNummer;
                        oSheet.Cells[currentrow, 9] = cursist.MobielNummer;
                        currentrow++;
                    }

                    //AutoFit columns
                    oRng = oSheet.get_Range("A1", "K1");
                    oRng.EntireColumn.AutoFit();

                    oXL.Visible = false;
                    oXL.UserControl = false;
                    oWB.SaveAs(savelocation + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                    false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    oWB.Close();
                }
                catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }
            
        }

        private void AlleCursistenJaarExcel()
        {


            string savelocation = Properties.Settings.Default.SavePath;
            string FileName = "Cursisten afgelopen jaar - "
            + " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            object misvalue = System.Reflection.Missing.Value;
            try
            {
                //Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                //verkrijg gegevens
                var test =
            zs.zs_cursusregel.ToList().Where(d=>d.begintijdendatum.Year==DateTime.Now.Year||
                d.eindtijdendatum.Year == DateTime.Now.Year
                ).Select(i => new
                {
                    Id = i.id,
                    Naam = i.zs_cursus.naam,
                    Plaatsen = i.zs_cursus.aantalplaatsenbeschikbaar,
                    Beschrijving = i.zs_cursus.beschrijving,
                    PlaatsenBezet= zs.zs_inschrijvingen.Where(ii=> ii.status=="Geaccepteerd" && ii.betaald==1 && ii.zs_cursusregel.id==i.id).GroupBy(ii=>ii.zs_cursusregel.id).Count()
                }
                );

                //zet headers
                int currentrow = 1;
                oSheet.Cells[currentrow, 1] = "Naam";
                oSheet.Cells[currentrow, 2] = "Aantal Plaatsen";
                oSheet.Cells[currentrow, 3] = "Plaatsen bezet";
                oSheet.Cells[currentrow, 4] = "Beschrijving";
                //zet headers bold
                oSheet.get_Range("A" + currentrow.ToString(), "Z" + currentrow.ToString()).Font.Bold = true;
                currentrow++;
                //zet gegevens
                foreach (var cursist in test)
                {
                    oSheet.Cells[currentrow, 1] = cursist.Naam;
                    oSheet.Cells[currentrow, 2] = cursist.Plaatsen;
                    oSheet.Cells[currentrow, 3] = cursist.PlaatsenBezet;
                    oSheet.Cells[currentrow, 4] = cursist.Beschrijving;
                    currentrow++;
                }

                //AutoFit columns
                oRng = oSheet.get_Range("A1", "K1");
                oRng.EntireColumn.AutoFit();

                oXL.Visible = false;
                oXL.UserControl = false;
                oWB.SaveAs(savelocation + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                oWB.Close();
            }
            catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }

        }

        private void InschrijvingenExcel()
        {


            string savelocation = Properties.Settings.Default.SavePath;
            string FileName = "Inschrijvingen - "
            + " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            object misvalue = System.Reflection.Missing.Value;
            try
            {
                //Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                //verkrijg gegevens
                var test =
            zs.zs_inschrijvingen.ToList().Select(i => new
            {
                Id = i.id,
                Gebruiker = i.zs_users.voornaam + " " + i.zs_users.tussenvoegsel + " " + i.zs_users.achternaam,
                Cursus = i.zs_cursusregel.zs_cursus.naam,
                Status = i.status,
                Datum = i.zs_cursusregel.begintijdendatum,
                EindDatum = i.zs_cursusregel.eindtijdendatum,
                DatumAangevraagd = i.datumaangevraagd
            }
            ).ToList();

                //zet headers
                int currentrow = 1;
                oSheet.Cells[currentrow, 1] = "Naam";
                oSheet.Cells[currentrow, 2] = "Cursus";
                oSheet.Cells[currentrow, 3] = "Status";
                oSheet.Cells[currentrow, 4] = "Datum";
                oSheet.Cells[currentrow, 5] = "EindDatum";
                oSheet.Cells[currentrow, 6] = "DatumAangevraagd";
                
                //zet headers bold
                oSheet.get_Range("A" + currentrow.ToString(), "Z" + currentrow.ToString()).Font.Bold = true;
                currentrow++;
                //zet gegevens
                foreach (var cursist in test)
                {
                    oSheet.Cells[currentrow, 1] = cursist.Gebruiker;
                    oSheet.Cells[currentrow, 2] = cursist.Cursus;
                    oSheet.Cells[currentrow, 3] = cursist.Status;
                    oSheet.Cells[currentrow, 4] = cursist.Datum;
                    oSheet.Cells[currentrow, 5] = cursist.EindDatum;
                    oSheet.Cells[currentrow, 6] = cursist.DatumAangevraagd;
                    currentrow++;
                }

                //AutoFit columns
                oRng = oSheet.get_Range("A1", "K1");
                oRng.EntireColumn.AutoFit();

                oXL.Visible = false;
                oXL.UserControl = false;
                oWB.SaveAs(savelocation + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                oWB.Close();
            }
            catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }

        }

        private void NieuweCursistenExcel()
        {


            string savelocation = Properties.Settings.Default.SavePath;
            string FileName = "Nieuwe Cursisten - "
            + " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            object misvalue = System.Reflection.Missing.Value;
            try
            {
                //Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                //verkrijg gegevens
                var test =
            zs.zs_users.ToList().Where(a =>a.DatumAangemaakt <= DateTime.Now && a.DatumAangemaakt >= DateTime.Now.AddMonths(-1)).Select(i => new
            {
                Id = i.id,
                Gebruiker = i.voornaam + " " + i.tussenvoegsel + " " + i.achternaam,
                Email = i.email,
                Geslacht = i.geslacht,
                GeboorteDatum = i.geboortedatum,
                Plaats = i.plaats,
                Straat = i.Vstraat,
                Postcode = i.Fpostcode,
                TelefoonNummer = i.telnummer,
                MobielNummer = i.mobnummer
            }
            ).ToList();

                //zet headers
                int currentrow = 1;
                oSheet.Cells[currentrow, 1] = "Naam";
                oSheet.Cells[currentrow, 2] = "Email";
                oSheet.Cells[currentrow, 3] = "Geslacht";
                oSheet.Cells[currentrow, 4] = "Geboortedatum";
                oSheet.Cells[currentrow, 5] = "Plaats";
                oSheet.Cells[currentrow, 6] = "Straat";
                oSheet.Cells[currentrow, 7] = "Postcode";
                oSheet.Cells[currentrow, 8] = "Telefoon nummer";
                oSheet.Cells[currentrow, 9] = "Mobiele nummer";
                //zet headers bold
                oSheet.get_Range("A" + currentrow.ToString(), "Z" + currentrow.ToString()).Font.Bold = true;
                currentrow++;
                //zet gegevens
                foreach (var cursist in test)
                {
                    oSheet.Cells[currentrow, 1] = cursist.Gebruiker;
                    oSheet.Cells[currentrow, 2] = cursist.Email;
                    oSheet.Cells[currentrow, 3] = cursist.Geslacht;
                    oSheet.Cells[currentrow, 4] = cursist.GeboorteDatum;
                    oSheet.Cells[currentrow, 5] = cursist.Plaats;
                    oSheet.Cells[currentrow, 6] = cursist.Straat;
                    oSheet.Cells[currentrow, 7] = cursist.Postcode;
                    oSheet.Cells[currentrow, 8] = cursist.TelefoonNummer;
                    oSheet.Cells[currentrow, 9] = cursist.MobielNummer;
                    currentrow++;
                }

                //AutoFit columns
                oRng = oSheet.get_Range("A1", "K1");
                oRng.EntireColumn.AutoFit();

                oXL.Visible = false;
                oXL.UserControl = false;
                oWB.SaveAs(savelocation + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                oWB.Close();
            }
            catch (Exception err) { System.Windows.MessageBox.Show(err.Message); }

        }
        //switch
        private void CONVERT(object sender, RoutedEventArgs e)
        {
            switch (huidigoverzicht) {
                case "Bezetting afgelopen jaar": 
                    AlleCursistenJaarExcel();
                    break;
                case "Alle cursisten":
                    AlleCursistenExcel();
                    break;
                case "Rooster medewerkers":
                    MedewerkerRoosterExcel();
                    break;
                case "Inschrijvingen":
                    InschrijvingenExcel();
                    break;
                case "Nieuwe Cursisten":
                    NieuweCursistenExcel();
                    break;
                default:
                    MessageBox.Show("niks geselecteerd");
                        break;
                
            }
            
            
           
        }





    }
}
