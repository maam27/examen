﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for TextPopup.xaml
    /// </summary>
    public partial class TextPopup : Window
    {
        public string returntext { get; set; }
        public TextPopup()
        {
            InitializeComponent();
        }      

        private void BTNaccept(object sender, RoutedEventArgs e)
        {
            returntext = stringtxt.Text;
            this.DialogResult = true;
            this.Close();
        }

        private void BTNcancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
