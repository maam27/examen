﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zeilschool
{
    partial class zs_onderhoud
    {
        public bool Overlap(zs_cursusregel s)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");

            if (s.begintijdendatum <= this.beginonderhoud && s.eindtijdendatum >= this.beginonderhoud) { return true; }
            if (s.begintijdendatum >= this.beginonderhoud && s.begintijdendatum <= this.eindonderhoud) { return true; }
            if (s.begintijdendatum <= this.eindonderhoud && s.eindtijdendatum >= this.eindonderhoud) { return true; }
            return false;
        }
    }
}
