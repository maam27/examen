﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Zeilschool
{
    class FTP
    {
        public static string[] SelectFiles()
        {
            System.Windows.Forms.OpenFileDialog openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.Filter = @"JPG|*.jpg|PNG|*.png|GIF|*.gif"; //use as you require.
            openFileDialog1.Multiselect = false;
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] fileNames = openFileDialog1.FileNames;
                return fileNames;
            }
            return null;
        }

        public static void UploadImage(String Subpath,string[] fileNames)
        {     
                foreach (var img in fileNames)
                {
                    String Filenamewithextension = System.IO.Path.GetFileName(img);
                    FtpWebRequest req = (FtpWebRequest)WebRequest.Create("ftp://10.9.115.22/images/upload/" + Subpath + Filenamewithextension);
                    req.UseBinary = true;
                    req.Method = WebRequestMethods.Ftp.UploadFile;
                    req.Credentials = new NetworkCredential("imageupload", "LOL");
                    byte[] fileData = File.ReadAllBytes(img);

                    req.ContentLength = fileData.Length;
                    Stream reqStream = req.GetRequestStream();
                    reqStream.Write(fileData, 0, fileData.Length);
                    reqStream.Close();
                }
        }
             
        public static IEnumerable<string> GetFilesInFtpDirectory(string url, string username, string password)
        {
            System.Net.FtpWebRequest ftpRequest = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(url);
            ftpRequest.Credentials = new System.Net.NetworkCredential(username, password);
            ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)ftpRequest.GetResponse();
            System.IO.StreamReader streamReader = new System.IO.StreamReader(response.GetResponseStream());

            List<string> directories = new List<string>();

            string line = streamReader.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                directories.Add(line);
                line = streamReader.ReadLine();
            }

            streamReader.Close();
            return directories;
        }
    }
}
