﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Zeilschool
{
    class Mail
    {
        public static void SendTo(String Reciever, String Subject, String Body)
        {
            string from = Properties.Settings.Default.ServerMail;
            MailMessage message = new MailMessage(from, Reciever, Subject, Body);
            SmtpClient client = new SmtpClient(Properties.Settings.Default.ServerIP);
            client.Timeout = 1000;        
            client.Credentials = CredentialCache.DefaultNetworkCredentials;
            message.IsBodyHtml = true;
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateTimeoutTestMessage(): {0}",ex.ToString());
            }
        }
    }
}
