﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zeilschool
{
    public partial class zs_cursusregel
    {      
        public bool Overlap(zs_cursusregel s)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
           
            if (s.begintijdendatum <= this.begintijdendatum && s.eindtijdendatum >= this.begintijdendatum) { return true; }
            if (s.begintijdendatum >= this.begintijdendatum && s.begintijdendatum <= this.eindtijdendatum) { return true; }
            if (s.begintijdendatum <= this.eindtijdendatum && s.eindtijdendatum >= this.eindtijdendatum) { return true; }
            return false;
        }

    }
}
