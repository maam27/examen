﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeilschool
{
    public partial class zs_users
    {
        public string Fpostcode { get { return string.Format("{1} {0}", this.postcode, this.postcode_cijfer); } }
        public string Vstraat { get { return string.Format("{0} {1:0}", this.straat, this.straatnr); } }
    }

}
