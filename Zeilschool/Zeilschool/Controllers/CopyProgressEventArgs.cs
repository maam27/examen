﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeilschool
{
   public  class CopyProgressEventArgs:EventArgs
    {

       public string name {get; set;}
       public double maximum { get; set; }
       public double progress { get; set; }


       public CopyProgressEventArgs(string name, double maximum, double progress)
       {
           this.name = name;
           this.maximum = maximum;
           this.progress = progress;
       }

    }
}
