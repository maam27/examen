﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Zeilschool
{
    class Controls
    {
        static public void ClearAllTextBoxes(Visual myMainWindow)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(myMainWindow);
            for (int i = 0; i < childrenCount; i++)
            {
                var visualChild = (Visual)VisualTreeHelper.GetChild(myMainWindow, i);
                if (visualChild is TextBox)
                {
                    TextBox tb = (TextBox)visualChild;
                    tb.Clear();
                }
                ClearAllTextBoxes(visualChild);
            }
        }

        static public void UnlockAllTextboxes(Visual myMainWindow)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(myMainWindow);
            for (int i = 0; i < childrenCount; i++)
            {
                var visualChild = (Visual)VisualTreeHelper.GetChild(myMainWindow, i);
                if (visualChild is TextBox)
                {
                    TextBox tb = (TextBox)visualChild;
                    tb.IsReadOnly = false;
                }
                UnlockAllTextboxes(visualChild);
            }
        }

        static public void LockAllTextboxes(Visual myMainWindow)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(myMainWindow);
            for (int i = 0; i < childrenCount; i++)
            {
                var visualChild = (Visual)VisualTreeHelper.GetChild(myMainWindow, i);
                if (visualChild is TextBox)
                {
                    TextBox tb = (TextBox)visualChild;
                    tb.IsReadOnly = true;
                }
                LockAllTextboxes(visualChild);
            }
        }

        static public void HideAllButtons(Visual myMainWindow)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(myMainWindow);
            for (int i = 0; i < childrenCount; i++)
            {
                var visualChild = (Visual)VisualTreeHelper.GetChild(myMainWindow, i);
                if (visualChild is Button)
                {
                    Button tb = (Button)visualChild;
                    tb.Visibility = Visibility.Hidden;
                }
                HideAllButtons(visualChild);
            }
        }

        public static void CheckIsDecimal(TextBox sender, TextCompositionEventArgs e)
        {
            decimal result;
            bool comma = sender.Text.IndexOf(",") < 0 && e.Text.Equals(",") && sender.Text.Length > 0;
            if (!(Decimal.TryParse(e.Text, out result) || comma))
            {
                e.Handled = true;
            }
        }

        public static void CheckIsNumeric(TextBox sender, TextCompositionEventArgs e)
        {
            int result;           
            if (!Int32.TryParse(e.Text, out result))
            {
                e.Handled = true;
            }
        }
    }
}
