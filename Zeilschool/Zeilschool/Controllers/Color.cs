﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Zeilschool
{
    class Color
    {
        /// <summary>
        /// return the main grid color of the settings file
        /// </summary>
        /// <returns></returns>
        public static Brush GridMain()
        {
            Brush B = new BrushConverter().ConvertFromString(Properties.Settings.Default.GridMainColor) as SolidColorBrush;
            return B;
        }
        /// <summary>
        /// return the secondary grid color of the settings file
        /// </summary>
        /// <returns></returns>
        public static Brush GridSecond()
        {
            Brush B = new BrushConverter().ConvertFromString(Properties.Settings.Default.GridSecondColor) as SolidColorBrush;
            return B;
        }
    }
}
