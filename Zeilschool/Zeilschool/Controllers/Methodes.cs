﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeilschool
{
    class Methodes
    {
        static zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        public static int GetGeaccepteerdeAanmeldingen(int cursusid) 
        {
            int aantal = zs.zs_inschrijvingen.Count(c => c.zs_cursusregel.id == cursusid && c.status == "Geaccepteerd");                
            return aantal;
        }
    }
}
