﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeilschool
{
   public static class Inschrijving
    {
       /// <summary>
       /// Deze methode zal een 0 of 1 bij betaald omzetten na een ja of nee
       /// </summary>
       /// <param name="Inschrijving">Object zs_inschrijvingen</param>
       /// <returns>Een ja of nee</returns>
       public static string GetBetaald(zs_inschrijvingen Inschrijving)
       {
           string betaald = "Nee";
           if (Inschrijving.betaald == 1)
           {
               betaald = "Ja";
           }

           return betaald;
       }

    }
}
