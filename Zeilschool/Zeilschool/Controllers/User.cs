﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;


namespace Zeilschool
{
    public static class User
    {

        /**
        * Written by Theproudwolf Aka Rick van Druten
        * User class to control user methods
        * */
        

        /// <summary>
        /// Vult een Listview met users
        /// </summary>
        /// <param name="listview">Stuur een object listview mee</param>
        public static void FillUserList(System.Windows.Controls.ListView listview)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            BindingSource Group = new BindingSource();

            Group.DataSource = zs.zs_users.Local;
            zs.zs_users.Load();

            listview.ItemsSource = null;
            listview.DisplayMemberPath = null;

            listview.ItemsSource = Group;
            listview.DisplayMemberPath = "email";
        }
        /// <summary>
        /// Krijg een object zs_users terug van een specifieke gebruiker, Je moet een int id meesturen
        /// </summary>
        /// <param name="id">Int id</param>
        /// <returns>Een object zs_users</returns>
        public static zs_users GetUserById(int id)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            zs_users user = zs.zs_users.SingleOrDefault(i => i.id == id);
            return user;
        }
        /// <summary>
        /// Deze methode zal een m omzetten naar de volledige geslacht van een gebruiker naar Man
        /// </summary>
        /// <param name="k">Object zs_users</param>
        /// <returns>Een string</returns>
        public static string GetGeslacht(zs_users k)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            String geslacht = "";

            if (k.geslacht == "m")
            {
                geslacht = "Man";
            }

            else if (k.geslacht == "v")
            {
                geslacht = "Vrouw";
            }
            else
            {
                geslacht = "Onbekend";
            }
            return geslacht;

        }

        /// <summary>
        /// Stuur een object zs_user en krijg de volledige naam + achternaam van een persoon terug als 1 string
        /// </summary>
        /// <param name="k">Object zs_users</param>
        /// <returns>Een String met de volledige naam van een gebruiker</returns>

        public static string GetUserFullName(zs_users k)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            string fullname = "";

            if (k.tussenvoegsel != null || k.tussenvoegsel != " " || k.tussenvoegsel != "")
            {
                fullname = k.voornaam + " " + k.tussenvoegsel + " " + k.achternaam;

            }
            else
            {
                fullname = k.voornaam + " " + k.achternaam;
            }



            return fullname;
        }


    }
}
