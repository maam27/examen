﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Data.Entity;


namespace Zeilschool
{
    /**
     * Written by Theproudwolf Aka Rick van Druten
     * Group class to control group methods
     * */


   public static class Group
    {

       /// <summary>
       /// Deze methode vult een combobox met groepen
       /// </summary>
       /// <param name="box">Een object combobox</param>
        public static void FillComboBox(System.Windows.Controls.ComboBox box)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            BindingSource Group = new BindingSource();

            Group.DataSource = zs.zs_groepen.Local;
            zs.zs_groepen.Load();

            box.ItemsSource = null;
            box.DisplayMemberPath = null;

            box.ItemsSource = Group;
            box.DisplayMemberPath = "naam";
        }

       /// <summary>
       /// Deze methode zorgt er voor dat de combobox gevuld word. En daarnaast heeft die een object klant nodig zodat de huidige rank van de klant als selecected item word ingesteld
       /// </summary>
       /// <param name="box">Een object combobox</param>
        /// <param name="user">Een object zs_users</param>
        public static void FillComboBox(System.Windows.Controls.ComboBox box,zs_users user)
        {
            zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
            BindingSource Group = new BindingSource();

            Group.DataSource = zs.zs_groepen.Local;
            zs.zs_groepen.Load();

            box.ItemsSource = null;
            box.DisplayMemberPath = null;

            box.ItemsSource = Group;
            box.DisplayMemberPath = "naam";

            if (Group.Count > 0) { box.SelectedIndex = 0; }

            if (user != null)
            {
                zs_groepen current = zs.zs_groepen.SingleOrDefault(g => g.id == user.groepen_id);
                box.SelectedItem = current;
            }
        }

    }
}
