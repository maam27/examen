﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Zeilschool
{
    class Simple
    {
        static zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        public static void SetScreenTopRight(Window screen, int WindowWidth)
        {
            screen.Left = System.Windows.SystemParameters.WorkArea.Width - WindowWidth;
            screen.Top = 0;
        }

        public static void SetScreenTopCenter(Window screen, int WindowWidth)
        {
            screen.Left = System.Windows.SystemParameters.WorkArea.Width / 2 - WindowWidth / 2;
            screen.Top = 0;
        }      
    }
}
