﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Cursusnaarbestand.xaml
    /// </summary>
    public partial class Cursusnaarbestand : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        private Int32 CursusID { get; set; }
        private string FileName { get; set; }

        public Cursusnaarbestand(Int32 CursusID)
        {
            InitializeComponent();
            this.CursusID = CursusID;
            FileName = "UnnamedFile.xls";
            path.Text = Properties.Settings.Default.SavePath;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //write options
            if (Excel.IsChecked == true)
            {
                WriteToExcel();
            }
            if (PDF.IsChecked == true)
            {
                WriteToPdf();
            }
        }

        private void WriteToPdf()
        {
            FileName = CursusID + " - " + DateTime.Now.ToShortDateString().ToString() + ".pdf";
            FileStream fs = new FileStream(path.Text + FileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Rectangle rec = new Rectangle(PageSize.A4);
            rec.BackgroundColor = new CMYKColor(0, 0, 0, 0);
            Document doc = new Document(rec);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();

            PdfPTable table = new PdfPTable(8);
            table.WidthPercentage = 100;

            #region cursus informatie



            zs_cursusregel Cursus = zs.zs_cursusregel.Where(c => c.id == CursusID).FirstOrDefault();
            zs_cursus cursusgegevens = zs.zs_cursus.Where(c => c.id == Cursus.cursus_id).FirstOrDefault();

            table.AddCell(NewCell("Datum: " + Environment.NewLine + "Tijd: ", 1));
            table.AddCell(NewCell(Cursus.begintijdendatum.ToString(), 1));
            table.AddCell(NewCell("Cursus naam", 2));
            table.AddCell(NewCell(cursusgegevens.naam, 4));

            table.AddCell(NewCell("Cursus Beschrijving:", 2));
            table.AddCell(NewCell(cursusgegevens.beschrijving, 6));

            table.AddCell(NewCell("Cursus prijs", 2));
            table.AddCell(NewCell("€" + string.Format("{0,00}", cursusgegevens.prijs.ToString()), 1));
            table.AddCell(NewCell("Plaatsen bezet", 2));
            table.AddCell(NewCell(Methodes.GetGeaccepteerdeAanmeldingen(CursusID).ToString() + "/", 1));
            table.AddCell(NewCell(cursusgegevens.aantalplaatsenbeschikbaar.ToString(), 2));

            table.AddCell(NewCell(" ", 8));

            //Boot
            table.AddCell(NewCellBorderBelow("Boot", 2));
            table.AddCell(NewCellBorderBelow("plekken", 2));
            table.AddCell(NewCellBorderBelow("beschrijving", 4));
            var link = zs.zs_bootlinktabel.Where(l => l.cursusregelid == Cursus.id).ToList();
            foreach (var linked in link)
            {
                var boten = zs.zs_boten.FirstOrDefault(b => b.id == linked.botenid);

                table.AddCell(NewCell(boten.naam, 2));
                table.AddCell(NewCell(boten.aantalplaatsen.ToString(), 2));
                table.AddCell(NewCell(boten.type, 4));
            }

            table.AddCell(NewCell(" ", 8));


            #endregion cursus informatie
            #region inschrijvingen
            table.AddCell(NewCellBorderBelow("Cursisten", 1));
            table.AddCell(NewCell(" ", 7));

            var Inschrijvingen = zs.zs_inschrijvingen.Where(i => i.status == "Geaccepteerd" && i.cursusregel_id == CursusID).OrderBy(i => i.zs_users.voornaam).ThenBy(i => i.zs_users.achternaam).ThenBy(i => i.zs_users.tussenvoegsel).ToList();
            table.AddCell(NewCellBorderBelow("Voornaam", 1));
            table.AddCell(NewCellBorderBelow("Tussen-" + Environment.NewLine + "voegsel", 1));
            table.AddCell(NewCellBorderBelow("Achternaam", 1));
            table.AddCell(NewCellBorderBelow("Plaats", 1));
            table.AddCell(NewCellBorderBelow("Straat", 2));
            table.AddCell(NewCellBorderBelow("Tel.", 1));
            table.AddCell(NewCellBorderBelow("Mobiel", 1));

            decimal winst = 0;
            foreach (var inschrijving in Inschrijvingen)
            {
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.voornaam.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.tussenvoegsel.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.achternaam.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.plaats.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.straat.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.straatnr.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.telnummer.ToString(), 1));
                table.AddCell(NewCellBorderBelow(inschrijving.zs_users.mobnummer.ToString(), 1));
                winst += inschrijving.zs_cursusregel.zs_cursus.prijs;
            }

            #endregion inschrijvingen
            table.AddCell(NewCell(" ", 8));
            table.AddCell(NewCell("Totale opbrengst", 2));
            table.AddCell(NewCell(string.Format("€{0:0.00}", winst), 6));

            doc.Add(table);
            doc.Close();

            this.Close();
        }

        private void WriteToExcel()
        {
            WriteExcel();
        }

        private void WriteExcel()
        {
            FileName = CursusID + " - " + DateTime.Now.ToShortDateString().ToString() + ".xlsx";
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            object misvalue = System.Reflection.Missing.Value;
            try
            {
                //Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;

                zs_cursusregel Cursus = zs.zs_cursusregel.Where(c => c.id == CursusID).FirstOrDefault();
                zs_cursus cursusgegevens = zs.zs_cursus.Where(c => c.id == Cursus.cursus_id).FirstOrDefault();

                oSheet.Cells[1, 1] = "Cursus id: ";
                oSheet.Cells[2, 1] = Cursus.id;
                oSheet.Cells[1, 2] = "Cursus Naam";
                oSheet.Cells[2, 2] = cursusgegevens.naam;

                oSheet.Cells[1, 3] = "Cursus prijs";
                oSheet.Cells[2, 3] = cursusgegevens.prijs;
                oSheet.Cells[2, 3].NumberFormat = "€0,00";
                oSheet.Cells[1, 4] = "Cursus plaatsen";
                oSheet.Cells[2, 4] = cursusgegevens.aantalplaatsenbeschikbaar;

                oSheet.Cells[1, 5] = "Cursus inschrijvingen";
                oSheet.Cells[2, 5] = Cursus.plaatsenbezet;
                oSheet.Cells[1, 6] = "Cursus datum";
                oSheet.Cells[2, 6] = Cursus.begintijdendatum;
                oSheet.Cells[1, 7] = "Cursus beschrijving";
                oSheet.Cells[2, 7] = cursusgegevens.beschrijving;

                oSheet.Cells[4, 1] = "vloot id";
                oSheet.Cells[4, 2] = "Vloot naam";
                oSheet.Cells[4, 3] = "Vloot maximale plekken";
                oSheet.Cells[4, 4] = "Vloot bouwjaar";
                oSheet.Cells[4, 5] = "In dienst sinds";
                oSheet.Cells[4, 6] = "Vloot beschrijving";

                int currentrow = 5;
                var link = zs.zs_bootlinktabel.Where(l => l.cursusregelid == Cursus.id).ToList();
                foreach (var linked in link)
                {
                    var boten = zs.zs_boten.FirstOrDefault(b => b.id == linked.botenid);

                    oSheet.Cells[currentrow, 1] = boten.id;
                    oSheet.Cells[currentrow, 2] = boten.naam;
                    oSheet.Cells[currentrow, 3] = boten.aantalplaatsen;
                    oSheet.Cells[currentrow, 6] = boten.beschrijving;
                    currentrow++;
                }
                currentrow++;


                var Inschrijving =
                from i in zs.zs_inschrijvingen
                where i.status == "Geaccepteerd" && i.cursusregel_id == Cursus.id
                orderby i.zs_cursusregel.begintijdendatum, i.cursusregel_id, i.zs_users.achternaam, i.zs_users.voornaam
                select new
                {
                    id = i.id,
                    gebruikerv = i.zs_users.voornaam,
                    gebruikerm = i.zs_users.tussenvoegsel,
                    gebruikera = i.zs_users.achternaam,
                    gebuikerplaats = i.zs_users.plaats,
                    gebruikerstraat = i.zs_users.straat,
                    gebruikersstraatnr = i.zs_users.straatnr,
                    gebruikertel = i.zs_users.telnummer,
                    gebruikermob = i.zs_users.mobnummer,
                    datum = i.zs_cursusregel.begintijdendatum
                };

                oSheet.Cells[currentrow, 1] = "Cursisten";
                currentrow++;
                oSheet.Cells[currentrow, 1] = "Voornaam";
                oSheet.Cells[currentrow, 2] = "Tussenvoegsel";
                oSheet.Cells[currentrow, 3] = "Achternaam";
                oSheet.Cells[currentrow, 4] = "Plaats";
                oSheet.Cells[currentrow, 5] = "Straatnaam";
                oSheet.Cells[currentrow, 6] = "Straat Nummer";
                oSheet.Cells[currentrow, 7] = "Telefoon Nummer";
                oSheet.Cells[currentrow, 8] = "Mobiele Nummer";

                oSheet.get_Range("A" + (currentrow - 1), "H" + currentrow).Font.Bold = true;
                oSheet.get_Range("A" + (currentrow - 1), "H" + currentrow).VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                currentrow++;

                if (Inschrijving != null)
                {
                    decimal winst = 0;
                    foreach (var inschrijving in Inschrijving)
                    {
                        oSheet.Cells[currentrow, 1] = inschrijving.gebruikerv;
                        oSheet.Cells[currentrow, 2] = inschrijving.gebruikerm;
                        oSheet.Cells[currentrow, 3] = inschrijving.gebruikera;
                        oSheet.Cells[currentrow, 4] = inschrijving.gebuikerplaats;
                        oSheet.Cells[currentrow, 5] = inschrijving.gebruikerstraat;
                        oSheet.Cells[currentrow, 6] = inschrijving.gebruikersstraatnr;
                        oSheet.Cells[currentrow, 7] = inschrijving.gebruikertel;
                        oSheet.Cells[currentrow, 8] = inschrijving.gebruikermob;
                        currentrow++;
                        winst += Cursus.zs_cursus.prijs;
                    }
                    currentrow++;

                    oSheet.Cells[currentrow, 1] = "Totale opbrengst";
                    oSheet.Cells[currentrow, 3] = string.Format("€{0:0.00}", winst);
                }

                oSheet.get_Range("A1", "H1").Font.Bold = true;
                oSheet.get_Range("A1", "H1").VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.get_Range("A4", "H4").Font.Bold = true;
                oSheet.get_Range("A4", "H4").VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                //AutoFit columns
                oRng = oSheet.get_Range("A1", "K1");
                oRng.EntireColumn.AutoFit();
                oXL.Columns[6].ColumnWidth = 20;
                oRng.EntireRow.AutoFit();




                oXL.Visible = false;
                oXL.UserControl = false;
                oWB.SaveAs(path.Text + FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                oWB.Close();
                this.Close();

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }


        private PdfPCell NewCell(String text, int colspan)
        {
            Font fontH1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            PdfPCell c = new PdfPCell(new Phrase(text, fontH1));
            c.BorderWidth = 0F;
            c.Colspan = colspan;
            return c;
        }
        private PdfPCell NewCellBorderBelow(String text, int colspan)
        {
            Font fontH1 = new Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
            PdfPCell c = new PdfPCell(new Phrase(text, fontH1));
            c.BorderWidth = 0F;
            c.BorderWidthBottom = 1F;
            c.Colspan = colspan;
            return c;
        }

    }
}
