﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for CursusPlanning.xaml
    /// </summary>
    public partial class CursusPlanning : Window
    {
        private string[] cursusafbeelding { get; set; }
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        private Boolean edit { get; set; }

        private int LastSelectedId { get; set; }

        public CursusPlanning()
        {
            InitializeComponent();
            ComboType();
            FillCursusList();
            var gridView = new GridView();
            this.CursusList.View = gridView;
            gridView.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("id") });
            gridView.Columns.Add(new GridViewColumn { Header = "Naam", DisplayMemberBinding = new Binding("naam") });
            gridView.Columns.Add(new GridViewColumn { Header = "Prijs", DisplayMemberBinding = new Binding("prijs") });

            this.CursusList.SelectionChanged += CursusList_SelectionChanged;
        }

        public void FillCursusList()
        {
            CursusList.Items.Clear();
            var cursus = from row in zs.zs_cursus select row;

            foreach (var v in cursus)
            {
                CursusList.Items.Add(v);
            }
        }

        public void ComboType()
        {
            Cursusbox.Items.Clear();
            var type = from row in zs.zs_bootgroeplink select row;

            foreach (var t in type)
            {
                Cursusbox.Items.Add(t.groepnaam);
            }
        }

        private void CursusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CursusList.SelectedIndex == -1 || edit == true) { return; }
            zs_cursus SelectedCursus = CursusList.SelectedItem as zs_cursus;

            LastSelectedId = SelectedCursus.id;
            CursusNaamTxt.Text = SelectedCursus.naam;
            CursusBeschrijvingTxt.Text = SelectedCursus.beschrijving;
            CursusprijsTxt.Text = SelectedCursus.prijs.ToString();
            CursusPlaatsenTxt.Text = SelectedCursus.aantalplaatsenbeschikbaar.ToString();
            Cursusbox.Text = SelectedCursus.zs_bootgroeplink.groepnaam;
            var img = zs.zs_cursusafbeelding.FirstOrDefault(c=> c.zs_cursus.id==SelectedCursus.id);
            imagepathtxt.Text = img.afbeelding;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CursusToevoegen(object sender, RoutedEventArgs e)
        {
            FillCursusList();
            Controls.ClearAllTextBoxes(this);
            Controls.UnlockAllTextboxes(this);
            HideButtons();
            cursussave.Visibility = Visibility.Visible;
            cursuscancel.Visibility = Visibility.Visible;

        }

        private void HideButtons()
        {
            cursustoevoegen.Visibility = Visibility.Hidden;
            cursusver.Visibility = Visibility.Hidden;
            cursusedit.Visibility = Visibility.Hidden;
            cursussave.Visibility = Visibility.Hidden;
            cursuscancel.Visibility = Visibility.Hidden;
            cursussaveedit.Visibility = Visibility.Hidden;
        }

        private void CursusVerwijderen(object sender, RoutedEventArgs e)
        {
            if (CursusList.SelectedIndex == -1) { return; }
            zs_cursus selectedItem = (zs_cursus)this.CursusList.SelectedItem;
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Weet je zeker dat u deze cursus wilt verwijderen?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                zs_cursus C = zs.zs_cursus.Where(Cursus => Cursus.id == selectedItem.id).FirstOrDefault();
                int id = C.id;
                zs_cursusafbeelding cursusimg = zs.zs_cursusafbeelding.FirstOrDefault(i => i.cursus_id == id);
                zs.zs_cursusafbeelding.Remove(cursusimg);
                zs.zs_cursus.Remove(C);
                zs.SaveChanges();
                FillCursusList();
            }
        }

        private void SaveCursus(object sender, RoutedEventArgs e)
        {
            if (!IsValidated()) { return; }
            Controls.LockAllTextboxes(this);
            HideButtons();
            cursustoevoegen.Visibility = Visibility.Visible;
            cursusedit.Visibility = Visibility.Visible;
            cursusver.Visibility = Visibility.Visible;
            edit = false;

            zs_cursus SelectedVloot = CursusList.SelectedItem as zs_cursus;
            zs_cursus Newcursus = new zs_cursus
            {
                beschrijving = CursusBeschrijvingTxt.Text,
                naam = CursusNaamTxt.Text,
                prijs = Convert.ToInt32(CursusprijsTxt.Text),
                aantalplaatsenbeschikbaar = Convert.ToInt32(CursusPlaatsenTxt.Text),
                bootgroep = zs.zs_bootgroeplink.Where(b => b.groepnaam == Cursusbox.SelectedValue).Select(b => b.id).FirstOrDefault(),
                aantalinstructeur = 0
            };

            zs.zs_cursus.Add(Newcursus);

            zs.SaveChanges();
            FillCursusList();
        }

        private Boolean IsValidated()
        {
            String ErrorMsg = "";

            if (string.IsNullOrWhiteSpace(CursusNaamTxt.Text)) { ErrorMsg += "Cursus naam mag niet leeg zijn" + Environment.NewLine; }
            if (string.IsNullOrWhiteSpace(CursusBeschrijvingTxt.Text)) { ErrorMsg += "Cursus beschrijving mag niet leeg zijn" + Environment.NewLine; }
            if (Convert.ToInt32(CursusPlaatsenTxt.Text) <= 0) { ErrorMsg += "Cursus mag niet minder dan 1 plek hebben." + Environment.NewLine; }
            if (Convert.ToDouble(CursusprijsTxt.Text) <= 0) { ErrorMsg += "Cursus prijs mag lager zijn dan 0 euro." + Environment.NewLine; }
            if (Cursusbox.SelectedIndex == -1) { ErrorMsg += "Er moet een boottype geselecteerd zijn." + Environment.NewLine; }

            //show error message
            if (!string.IsNullOrWhiteSpace(ErrorMsg)) { MessageBox.Show("Foutmelding bericht" + Environment.NewLine + ErrorMsg); return false; }
            return true;
        }

        private void CursusEdit(object sender, RoutedEventArgs e)
        {
            if (CursusList.SelectedIndex == -1) { return; }
            edit = true;

            Controls.UnlockAllTextboxes(this);
            HideButtons();


            cursussaveedit.Visibility = Visibility.Visible;
            cursuscancel.Visibility = Visibility.Visible;
        }

        private void SaveCursusEdit(object sender, RoutedEventArgs e)
        {
            if (!IsValidated()) { return; }
            HideButtons();
            Controls.LockAllTextboxes(this);
            cursustoevoegen.Visibility = Visibility.Visible;
            cursusedit.Visibility = Visibility.Visible;
            cursusver.Visibility = Visibility.Visible;

            zs_cursus SelectedCursus = (zs_cursus)this.CursusList.SelectedItems[0];
            edit = false;

            //update huidige data
            SelectedCursus.naam = CursusNaamTxt.Text;
            SelectedCursus.beschrijving = CursusBeschrijvingTxt.Text;
            SelectedCursus.aantalplaatsenbeschikbaar = Convert.ToInt32(CursusPlaatsenTxt.Text);
            SelectedCursus.prijs = Convert.ToDecimal(CursusprijsTxt.Text);
            SelectedCursus.aantalinstructeur = 0;
            var HuidigeBootGroep = zs.zs_bootgroeplink.FirstOrDefault(b => b.groepnaam == Cursusbox.SelectedItem); //Cursusbox.SelectedItem as zs_bootgroeplink;
            SelectedCursus.bootgroep = HuidigeBootGroep.id;


            zs.SaveChanges();
            FillCursusList();
        }

        private void CursusCancel(object sender, RoutedEventArgs e)
        {
            HideButtons();
            Controls.LockAllTextboxes(this);
            cursustoevoegen.Visibility = Visibility.Visible;
            cursusedit.Visibility = Visibility.Visible;
            cursusver.Visibility = Visibility.Visible;
            edit = false;
        }

        private void Nummers(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsNumeric((TextBox)sender, e);
        }

        private void Decimaal(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsDecimal((TextBox)sender, e);
        }

        private void zoekafbeelding(object sender, RoutedEventArgs e)
        {
            cursusafbeelding = FTP.SelectFiles();
            foreach (var imgpath in cursusafbeelding)
            {
                CursusBeschrijvingTxt.Text = imgpath;
            }
        }

        private void wijzigimg(object sender, RoutedEventArgs e)
        {
            if (CursusList.SelectedIndex == -1) { return; }
            zs_cursus SelectedCursus = (zs_cursus)this.CursusList.SelectedItems[0];
            zs_cursusafbeelding afb = zs.zs_cursusafbeelding.FirstOrDefault(afbeelding => afbeelding.zs_cursus.id == SelectedCursus.id);

            cursusafbeelding = FTP.SelectFiles();
            try
            {
                foreach (var img in cursusafbeelding)
                {
                    if (FTP.GetFilesInFtpDirectory("ftp://10.9.115.22/images/upload/cursus/", "imageupload", "LOL").Contains(img))
                    {
                        afb.afbeelding = String.Format("{0}{1}", "http://10.9.115.22/images/upload/cursus/", System.IO.Path.GetFileName(img));
                    }
                    else
                    {
                        afb.afbeelding = String.Format("{0}{1}", "http://10.9.115.22/images/upload/cursus/", System.IO.Path.GetFileName(img));
                        FTP.UploadImage("cursus/", cursusafbeelding);
                        break;
                    }
                }
                zs.SaveChanges();
            }
            catch (Exception err) { }
        }
    }
}
