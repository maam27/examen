﻿using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Controls;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows;
using System.Data;
using System.Linq;
using System.Text;
using System;
using System.Globalization;
using System.Threading;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Cursussen.xaml
    /// </summary>
    public partial class Cursussen : Window
    {
        private string[] cursusafbeelding { get; set; }
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        private int CurrentFleet { get; set; }
        private Boolean edit { get; set; }
        public Cursussen()
        {

            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");

            edit = false;
            var cursusvieuw = new GridView();
            this.CursusList.View = cursusvieuw;
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("Id") });
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Cursus naam", DisplayMemberBinding = new Binding("Naam") });
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Datum", DisplayMemberBinding = new Binding("Datum") });

            FillCursusList();
            var gridView = new GridView();
            this.VlootList.View = gridView;
            gridView.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("id") });
            gridView.Columns.Add(new GridViewColumn { Header = "Naam", DisplayMemberBinding = new Binding("naam") });
            gridView.Columns.Add(new GridViewColumn { Header = "Aantal", DisplayMemberBinding = new Binding("aantalplaatsen") });

            var grid2View = new GridView();
            this.Instructeurlist.View = grid2View;
            grid2View.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("id") });
            grid2View.Columns.Add(new GridViewColumn { Header = "Voornaam", DisplayMemberBinding = new Binding("voornaam") });
            grid2View.Columns.Add(new GridViewColumn { Header = "Tussenvoegsel", DisplayMemberBinding = new Binding("tussenvoegsel") });
            grid2View.Columns.Add(new GridViewColumn { Header = "Achternaam", DisplayMemberBinding = new Binding("achternaam") });

            this.CursusList.SelectionChanged += CursusList_SelectionChanged;
        }

        //Boot lijst vullen
        public void FillFleetList()
        {
            VlootList.Items.Clear();
            var vloot = from row in zs.zs_boten select row;

            foreach (var v in vloot)
            {
                VlootList.Items.Add(v);
            }

        }

        //vernieuw de cursus lijst
        public void FillCursusList()
        {
            CursusList.Items.Clear();
            var cursus =
            zs.zs_cursusregel.Select(i => new
            {
                Id = i.id,
                Naam = i.zs_cursus.naam,
                Datum = i.begintijdendatum
            }
            ).ToList();

            foreach (var c in cursus)
            {
                CursusList.Items.Add(c);
            }
        }

        //laat gegevens van geselecteerde cursus zien
        private void CursusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CursusList.SelectedIndex == -1 || edit == true) { return; }

            dynamic selectedItem = CursusList.SelectedItem;
            int id = selectedItem.Id;

            zs_cursusregel cursus = zs.zs_cursusregel.SingleOrDefault(i => i.id == id);
            if (cursus != null)
            {
                CursusNaamTxt.Text = cursus.zs_cursus.naam;
                CursusBeschrijvingTxt.Text = cursus.zs_cursus.beschrijving;
                CursusPrijsTxt.Text = String.Format("€ {0:0.00}", cursus.zs_cursus.prijs);
                CursusplaatsenbezetTxt.Text = String.Format("{0}/{1}", zs.zs_inschrijvingen.Count(c => c.zs_cursusregel.id == id && c.status == "Geaccepteerd").ToString(), cursus.zs_cursus.aantalplaatsenbeschikbaar.ToString());
                CursusDate.SelectedDate = cursus.begintijdendatum;
                beginuur.Text = cursus.begintijdendatum.Hour.ToString();
                beginmin.Text = String.Format("{0:00}", cursus.begintijdendatum.Minute);

                CursusEindDate.SelectedDate = cursus.eindtijdendatum;
                einduur.Text = cursus.eindtijdendatum.Hour.ToString();
                eindmin.Text = String.Format("{0:00}", cursus.eindtijdendatum.Minute);

                this.VlootList.Items.Clear();

                if (cursus.zs_bootlinktabel.Count() > 0)
                {
                    foreach (var boot in cursus.zs_bootlinktabel)
                    {
                        zs_boten b = zs.zs_boten.FirstOrDefault(bl => bl.id == boot.botenid);
                        VlootList.Items.Add(b);
                    }
                }

                this.Instructeurlist.Items.Clear();
                if (cursus.zs_geplandeinstructeurs.Count() > 0)
                {
                    foreach (var instruct in cursus.zs_geplandeinstructeurs)
                    {
                        zs_users u = zs.zs_users.FirstOrDefault(bl => bl.id == instruct.user_id);
                        this.Instructeurlist.Items.Add(u);
                    }
                }
            }
        }

        //staat het inplannen van een nieuwe cursus toe
        private void NewCursus(object sender, RoutedEventArgs e)
        {
            //open new window
            PlanCursus pc = new PlanCursus();
            pc.Owner = this;
            pc.ShowDialog();
            FillCursusList();
        }

        //open nieuw scherm
        private void ToFile(object sender, RoutedEventArgs e)
        {
            if (CursusList.SelectedIndex != -1)
            {
                if (!string.IsNullOrEmpty(CursusList.SelectedItem.ToString()))
                {
                    if (this.CursusList.SelectedItems.Count != 1) { return; }
                    dynamic selected = this.CursusList.SelectedItem;
                    Cursusnaarbestand bestand = new Cursusnaarbestand(Convert.ToInt32(selected.Id));
                    bestand.Owner = Window.GetWindow(this);
                    bestand.ShowDialog();
                }
            }
        }

        
        private void RemoveCursus(object sender, RoutedEventArgs e)
        {
            if (this.CursusList.SelectedItems.Count != 1) { return; }
            dynamic selectedcursus = this.CursusList.SelectedItem;
            int cursusid = selectedcursus.Id;

            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Weet je zeker dat u deze geplande cursus wilt verwijderen, alle inschrijvingen voor deze cursus worden geannuleerd?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                VerwijderCursusReden va = new VerwijderCursusReden();
                va.Owner = this;
                va.ShowDialog();
                string reden = va.reden;

                //verkrijg cursus regel
                zs_cursusregel cursusregel = zs.zs_cursusregel.FirstOrDefault(cr => cr.id == cursusid);
                //verkrijg inschrijvingen voor de cursusregel
                var inschrijvingen = zs.zs_inschrijvingen.Where(inschrijf => inschrijf.cursusregel_id == cursusregel.id).ToList();
                //gebruiker een annulerings mail versturen
                foreach (var item in inschrijvingen)
                {
                    Mail.SendTo(
                item.zs_users.email,
                string.Format("Inschrijving '{0}'", item.zs_cursusregel.zs_cursus.naam),
@"
<html>

<style>
body {background-color:lightgrey;}
.Page{
width:750px;
height:auto;
}
.Top {
background-color:#0096D9;
width:750px;
height:78px;
margin:0px;
}
.Middle {
background-color:white;
width:745px;
padding-left:5px;
padding-bottom:5px;
padding-top:2px;
height:auto;
margin:0px;}
.Bottom {
background-color:#0096D9;
width:750px;
height:78px;
}
.plaatje {
margin-left:550px;
}
.bottomtext {
margin-left:550px;
}
</style>
" + String.Format(@"
<div class='Page'>
	<div class='Top'>
			<img src='http://10.9.115.22/images/LogoSmall.png' />
	</div>
	<div class='Middle'>
		<h1>Beste, {1}</h1>
		<p>Bij deze willen wij u informeren dat de cursus waar u zich voor hebt ingeschreven is geannuleerd.</p>
		<p>Dit betreft '{2}' die zou beginnen op {3:dd-MM-yyyy} om {3:hh:mm}.</p>
        <p>De reden hiervoor is: {4}</p>
        <p>Indien u al voor de cursus heeft betaald wordt dit zo spoedig mogelijk teruggestort.</p>
		<p>Met vriendelijke groet,</p>
        <p>Zeilschool de Waai</p>
	<p class='bottomtext'>		
		De Waai<br>
		NanneWiid 12<br>
		8314 PM Earnewoude<br>
		05134 56 78 34<br>
		DeWaai@hotmail.com
	</p>
	</div>	
	<div class='Bottom'>
		<div class='plaatje'>
			<img  src='http://10.9.115.22/images/LogoSmall.png' />
		</div>
	</div>
</div>
</html>
",
         "",
         item.zs_users.voornaam + " " + item.zs_users.tussenvoegsel + " " + item.zs_users.achternaam,
         item.zs_cursusregel.zs_cursus.naam,
        item.zs_cursusregel.begintijdendatum,
        reden
         ));
                }
                //verwijder alle inschrijvingen
                foreach (var item in inschrijvingen)
                {
                    zs.zs_inschrijvingen.Remove(item);
                }

                var boten = zs.zs_bootlinktabel.Where(link => link.cursusregelid == cursusregel.id).ToList();
                foreach (var boot in boten)
                {
                    zs.zs_bootlinktabel.Remove(boot);
                }

                var instructeurs = zs.zs_geplandeinstructeurs.Where(link => link.cursusregel_id == cursusregel.id).ToList();
                foreach (var instruct in instructeurs)
                {
                    zs.zs_geplandeinstructeurs.Remove(instruct);
                }

                //verwijder cursusregel
                zs.zs_cursusregel.Remove(cursusregel);
                //sla wijzigingen op
                zs.SaveChanges();
                //vernieuw cursus lijst
                FillCursusList();
            }
        }

        private void Edit(object sender, RoutedEventArgs e)
        {
            BtnNew.Visibility = Visibility.Hidden;
            BtnWijzig.Visibility = Visibility.Hidden;
            BtnFile.Visibility = Visibility.Hidden;
            BtnRemove.Visibility = Visibility.Hidden;
            beginuur.IsReadOnly = false;
            beginmin.IsReadOnly = false;
            einduur.IsReadOnly = false;
            eindmin.IsReadOnly = false;

            BtnSaveChange.Visibility = Visibility.Visible;
            BtnCancel.Visibility = Visibility.Visible;
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            BtnNew.Visibility = Visibility.Visible;
            BtnWijzig.Visibility = Visibility.Visible;
            BtnFile.Visibility = Visibility.Visible;
            BtnRemove.Visibility = Visibility.Visible;
            beginuur.IsReadOnly = true;
            beginmin.IsReadOnly = true;
            einduur.IsReadOnly = true;
            eindmin.IsReadOnly = true;

            BtnSaveChange.Visibility = Visibility.Hidden;
            BtnCancel.Visibility = Visibility.Hidden;
        }

        //sla wijzigingen op
        private void SaveChanges(object sender, RoutedEventArgs e)
        {
            if (!IsValidated()) { return; }
            beginuur.IsReadOnly = true;
            beginmin.IsReadOnly = true;
            einduur.IsReadOnly = true;
            eindmin.IsReadOnly = true;
            BtnNew.Visibility = Visibility.Visible;
            BtnWijzig.Visibility = Visibility.Visible;
            BtnFile.Visibility = Visibility.Visible;
            BtnRemove.Visibility = Visibility.Visible;

            BtnSaveChange.Visibility = Visibility.Hidden;
            BtnCancel.Visibility = Visibility.Hidden;

            dynamic selectedItem = CursusList.SelectedItem;
            int id = selectedItem.Id;

            zs_cursusregel cursus = zs.zs_cursusregel.SingleOrDefault(i => i.id == id);

            //update huidige data
            DateTime begindate = CursusDate.SelectedDate.Value;
            TimeSpan ts = new TimeSpan(Convert.ToInt32(beginuur.Text), Convert.ToInt32(beginmin.Text), 0);
            begindate = begindate.Date + ts;

            DateTime einddate = CursusEindDate.SelectedDate.Value;
            TimeSpan ts2 = new TimeSpan(Convert.ToInt32(einduur.Text), Convert.ToInt32(eindmin.Text), 0);
            einddate = einddate.Date + ts2;

            cursus.begintijdendatum = begindate;
            cursus.eindtijdendatum = einddate;

            zs.SaveChanges();
            FillCursusList();
            CursusDate.Focusable = false;
        }

        private Boolean IsValidated()
        {
            String ErrorMsg = "";
            if (CursusDate.SelectedDate <= DateTime.Now) { ErrorMsg += "De cursus kan niet op de zelfde dag als het aanmaken of ervoor zijn." + Environment.NewLine; }

            if (String.IsNullOrWhiteSpace(beginuur.Text) || String.IsNullOrWhiteSpace(beginmin.Text) || String.IsNullOrWhiteSpace(einduur.Text) || String.IsNullOrWhiteSpace(eindmin.Text)) { ErrorMsg += "U heeft een of meer tijden niet ingevoerd" + Environment.NewLine; }
            if ((Convert.ToInt32(beginmin.Text) > 59 && Convert.ToInt32(beginuur.Text) > 23) &&
               (Convert.ToInt32(eindmin.Text) > 59 && Convert.ToInt32(einduur.Text) > 23))
            { ErrorMsg += "U heeft een ongeldige tijd ingevoerd." + Environment.NewLine; }

            //show error message
            if (!string.IsNullOrWhiteSpace(ErrorMsg)) { MessageBox.Show("Foutmelding bericht" + Environment.NewLine + ErrorMsg); return false; }
            return true;
        }

        private void DecimalInputCheck(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsDecimal((TextBox)sender, e);
        }

        private void NumericInputCheck(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsNumeric((TextBox)sender, e);
        }    
    }
}
