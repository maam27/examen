﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Linq;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        #region lock close button
        [DllImport("user32.dll")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32.dll")]
        static extern bool EnableMenuItem(IntPtr hMenu, uint uIDEnableItem, uint uEnable);
        const uint MF_BYCOMMAND = 0x00000000;
        const uint MF_GRAYED = 0x00000001;
        const uint MF_ENABLED = 0x00000000;
        const uint SC_CLOSE = 0xF060;
        const int WM_SHOWWINDOW = 0x00000018;
        #endregion

        public Login()
        {
            InitializeComponent();
            email.Focus();
        }

        private void TryLogin(object sender, RoutedEventArgs e)
        {
            login();
        }

        private void login()
        {           
            string md5wachtwoord = Security.encrypt(password.Password);

            try
            {
                var User = zs.zs_users.Where(u => u.email == email.Text && u.password == md5wachtwoord).FirstOrDefault();

                var Group = zs.zs_groepen.Where(id => id.id == User.groepen_id).FirstOrDefault();
                if (Group.canloginonapp == true)
                {
                    CurrentUser.Current = User;
                    this.Close();
                }
                else
                {
                    if (Group.canloginonapp == false) 
                    {
                        errorlabel.Content = "U heeft niet de juiste rechten om in te loggen";
                    }
                    else
                    {
                        errorlabel.Content = "Login mislukt";
                    }
                }
            }
            catch (Exception e)
            {
                errorlabel.Content = "Login mislukt";
            }
        }

        private void CancelLogin(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void LoginBypass(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //CurrentUser.Current = zs.zs_users.FirstOrDefault(u=>u.id==24);
            //this.Close();
        }
        private void Formkeypress(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                login();
            }
        }

        #region lock close button
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;

            if (hwndSource != null)
            {
                hwndSource.AddHook(new HwndSourceHook(this.hwndSourceHook));
            }
        }

        IntPtr hwndSourceHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_SHOWWINDOW)
            {
                IntPtr hMenu = GetSystemMenu(hwnd, false);
                if (hMenu != IntPtr.Zero)
                {
                    EnableMenuItem(hMenu, SC_CLOSE, MF_BYCOMMAND | MF_GRAYED);
                }
            }
            return IntPtr.Zero;
        }
        #endregion lock close button
    }
}
