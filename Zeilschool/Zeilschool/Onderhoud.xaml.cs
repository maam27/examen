﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Onderhoud.xaml
    /// </summary>
    public partial class Onderhoud : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        public Onderhoud()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");


            FillFleetList();
            var gridView = new GridView();
            this.VlootList.View = gridView;
            gridView.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("id") });
            gridView.Columns.Add(new GridViewColumn { Header = "Name", DisplayMemberBinding = new Binding("naam") });
            gridView.Columns.Add(new GridViewColumn { Header = "Boot type", DisplayMemberBinding = new Binding("type") });

            FillOnderhoudList();
            var gridView2 = new GridView();
            this.OnderhoudList.View = gridView2;
            gridView2.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("id") });
            gridView2.Columns.Add(new GridViewColumn { Header = "Boot Naam", DisplayMemberBinding = new Binding("vlootnaam") });
            gridView2.Columns.Add(new GridViewColumn { Header = "Boot type", DisplayMemberBinding = new Binding("type") });
            gridView2.Columns.Add(new GridViewColumn { Header = "Begin Datum", DisplayMemberBinding = new Binding("beginonderhoud") });
            gridView2.Columns.Add(new GridViewColumn { Header = "Eind Datum", DisplayMemberBinding = new Binding("eindonderhoud") });
        }

        public void FillFleetList()
        {
            VlootList.Items.Clear();
            var vloot = from row in zs.zs_boten select row;

            foreach (var v in vloot)
            {
                VlootList.Items.Add(v);
            }
        }

        public void FillOnderhoudList()
        {
            OnderhoudList.Items.Clear();
            var onder = zs.zs_onderhoud.Select(o => new
            {
                id = o.id,
                vloot_id = o.vloot_id,
                beginonderhoud = o.beginonderhoud,
                eindonderhoud = o.eindonderhoud,
                vlootnaam = o.zs_boten.naam,
                type = o.zs_boten.type
            });

            foreach (var o in onder)
            {
                OnderhoudList.Items.Add(o);
            }
        }

        private void VoegToe(object sender, RoutedEventArgs e)
        {
            if (VlootList.SelectedIndex == -1) { return; }
            zs_boten SelectedVloot = VlootList.SelectedItem as zs_boten;

            var screen = new Onderhouddatum();
            screen.Left = this.Left + this.Width;
            screen.Top = this.Top;
            if (screen.ShowDialog() == true)
            {
                DateTime begin = screen.date1;
                DateTime eind = screen.date2;

                // save naar db
                zs_onderhoud onderhoud = new zs_onderhoud();
                onderhoud.beginonderhoud = begin;
                onderhoud.eindonderhoud = eind;
                onderhoud.vloot_id = SelectedVloot.id;

                zs.zs_onderhoud.Add(onderhoud);
                zs.SaveChanges();
            }

            FillOnderhoudList();
        }

        private void UitOnderhoud(object sender, RoutedEventArgs e)
        {
            if (OnderhoudList.SelectedIndex == -1) { return; }
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Weet u zeker dat u deze vloot uit onderhoud wil verwijderen?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                dynamic selectedItem = OnderhoudList.SelectedItem;
                int selectedid = selectedItem.id;
                zs_onderhoud C = zs.zs_onderhoud.Where(Onderhoud => Onderhoud.id == selectedid).FirstOrDefault();
                zs.zs_onderhoud.Remove(C);
                zs.SaveChanges();
                FillFleetList();
                FillOnderhoudList();
            }
        }

        private void Bewerken(object sender, RoutedEventArgs e)
        {
            if (OnderhoudList.SelectedIndex == -1) { return; }
            dynamic selectedItem = OnderhoudList.SelectedItem;

            DateTime d1 = selectedItem.beginonderhoud;
            DateTime d2 = selectedItem.eindonderhoud;

            var screen = new Onderhouddatum(d1, d2);
            screen.Left = this.Left + this.Width;
            screen.Top = this.Top;
            if (screen.ShowDialog() == true)
            {
                int selectedOnderhoud = selectedItem.id;
                zs_onderhoud update = zs.zs_onderhoud.Where(o => o.id == selectedOnderhoud).FirstOrDefault();
                update.beginonderhoud = screen.date1;
                update.eindonderhoud = screen.date2;
                zs.SaveChanges();
                FillOnderhoudList();
            }
        }
    }
}
