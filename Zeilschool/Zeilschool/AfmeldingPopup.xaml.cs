﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for AfmeldingPopup.xaml
    /// </summary>
    public partial class AfmeldingPopup : Window
    {
        public string redentext { get; set; }
        public AfmeldingPopup()
        {
            InitializeComponent();
        }

        private void Accept(object sender, RoutedEventArgs e)
        {
            if (cursusvol.IsChecked == true) 
            { 
                redentext="De cursus zit helaas vol.";
            }

            if (diploma.IsChecked == true)
            {
                redentext = "Uw huidige zwemdiploma is niet goed genoeg voor deze cursus.";
            }

            if (cancel.IsChecked == true)
            {
                redentext = "Deze cursus is geannuleerd.";
            }

            if (reden.IsChecked == true)
            {                
                redentext = redenbox.Text;
            }

            this.DialogResult = true;
            this.Close();
        }

        private void toggletext(object sender, RoutedEventArgs e)
        {
            if (reden.IsChecked == true) { redenbox.Focusable = true; } 
            else { redenbox.Focusable = false; }
        }
    }
}
