﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        public MainWindow()
        {
            InitializeComponent();
            Login l = new Login();
            l.ShowDialog();
            FillGrid();
        }

        //datagrid vullen
        public void FillGrid()
        {
            var test =
                zs.zs_inschrijvingen.Select(i => new
                {
                    Id = i.id,
                    Gebruiker = i.zs_users.voornaam + " " + i.zs_users.tussenvoegsel + " " + i.zs_users.achternaam,
                    Cursus = i.zs_cursusregel.zs_cursus.naam,
                    Status = i.status,
                    Datum = i.zs_cursusregel.begintijdendatum,
                    DatumAangevraagd = i.datumaangevraagd
                }
                ).Where(i => i.Status == "Wordt verwerkt").OrderBy(o=> o.DatumAangevraagd).ToList();

            try
            {
                aanmeldinggrid.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }

            this.aanmeldinggrid.RowBackground = Color.GridMain();
            this.aanmeldinggrid.AlternatingRowBackground = Color.GridSecond();
        }

        //aanmelding verwerken
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (aanmeldinggrid.SelectedIndex != -1)
            {
                object item = aanmeldinggrid.SelectedItem;
                int ID = Convert.ToInt32((aanmeldinggrid.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text);

                VerwerkAanmelding va = new VerwerkAanmelding(ID);
                va.Left = this.Left + this.Width;
                va.Top = this.Top;
                va.ShowDialog();
                FillGrid();
            }
        }

        //refresh button
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FillGrid();
        }

        private void OpenPermissionScreen(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canManagePermissions == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            PermissionSystem perm = new PermissionSystem();
            perm.Left = this.Left + this.Width;
            perm.Top = this.Top;
            perm.Show();
        }

        private void OpenCurssusScreen(object sender, RoutedEventArgs e)
        {
            if(CurrentUser.Current.zs_groepen.canCreateCursus==false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            CursusPlanning cursus = new CursusPlanning();
            cursus.Left = this.Left + this.Width;
            cursus.Top = this.Top;
            cursus.Show();
        }

        private void OpenCurssusPlanningScreen(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canPlanCursus == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            Cursussen cursus = new Cursussen();
            cursus.Left = this.Left + this.Width;
            cursus.Top = this.Top;
            cursus.Show();
        }

        private void VlotenBeheer(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canMangeBoats == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            Vlotenbeheer vb = new Vlotenbeheer();
            vb.Left = this.Left + this.Width;
            vb.Top = this.Top;
            vb.Show();
        }

        private void Onderhoud(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canManageMaintainance == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            Onderhoud oh = new Onderhoud();
            oh.Left = this.Left + this.Width;
            oh.Top = this.Top;
            oh.Show();
        }

        private void klantbeheer(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canManageCursisten == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            KlantenBeheer kb = new KlantenBeheer();
            kb.Left = this.Left + this.Width;
            kb.Top = this.Top;
            kb.Show();
        }

        private void Overzicht(object sender, RoutedEventArgs e)
        {
            if (CurrentUser.Current.zs_groepen.canSeeOverview == false)
            {
                MessageBox.Show("Onvoldoende rechten");
                return;
            }
            Overzicht oz = new Overzicht();
            oz.Left = this.Left + this.Width;
            oz.Top = this.Top;
            oz.Show();
        }

        private void logout(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }      
    }
}
