﻿using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows;
using System.Linq;
using System.Text;
using System;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for PlanCursus.xaml
    /// </summary>
    public partial class PlanCursus : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        public PlanCursus()
        {
            InitializeComponent();
            einddatum.SelectedDate = DateTime.Now;
            begindatum.SelectedDate = DateTime.Now;

            var cursusvieuw = new GridView();
            this.cursusselected.View = cursusvieuw;
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Id", DisplayMemberBinding = new Binding("Id") });
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Cursus naam", DisplayMemberBinding = new Binding("Naam") });
            cursusvieuw.Columns.Add(new GridViewColumn { Header = "Beschrijving", DisplayMemberBinding = new Binding("Beschrijving") });

            filllists();
        }

        private void filllists()
        {
            FillCursusList();
        }

        //vernieuw de cursus lijst
        public void FillCursusList()
        {
            this.cursusselected.Items.Clear();
            var cursus =
            zs.zs_cursus.Select(i => new
            {
                Id = i.id,
                Naam = i.naam,
                Beschrijving = i.beschrijving
            }
            ).ToList();

            foreach (var c in cursus)
            {
                this.cursusselected.Items.Add(c);
            }
        }

        private void save(object sender, RoutedEventArgs e)
        {
            if (this.cursusselected.SelectedIndex != -1)
            {
                dynamic cursus = this.cursusselected.SelectedItem;
                int cid = cursus.Id;

                if (String.IsNullOrWhiteSpace(beginuur.Text) ||
                    String.IsNullOrWhiteSpace(beginmin.Text) ||
                    String.IsNullOrWhiteSpace(einduur.Text) ||
                    String.IsNullOrWhiteSpace(eindmin.Text))
                { return; }
                if (Convert.ToInt32(beginuur.Text) > 24) { return; }
                if (Convert.ToInt32(beginmin.Text) > 60) { return; }
                if (Convert.ToInt32(einduur.Text) > 24) { return; }
                if (Convert.ToInt32(eindmin.Text) > 60) { return; }

                DateTime begindate = begindatum.SelectedDate.Value;
                TimeSpan ts = new TimeSpan(Convert.ToInt32(beginuur.Text), Convert.ToInt32(beginmin.Text), 0);
                begindate = begindate.Date + ts;
                DateTime einddate = einddatum.SelectedDate.Value;
                TimeSpan ts2 = new TimeSpan(Convert.ToInt32(einduur.Text), Convert.ToInt32(eindmin.Text), 0);
                einddate = einddate.Date + ts2;
                //eind gegevens validatie

                zs_cursusregel regel = new zs_cursusregel();
                regel.cursus_id = cid;
                regel.begintijdendatum = begindate;
                regel.eindtijdendatum = einddate;
                regel.vloot_id = 0;

                try
                {
                    #region
                    ////validatie voor de boot
                    var t = zs.zs_boten.Count();

                    List<zs_boten> templist = new List<zs_boten>();

                    foreach (var item in zs.zs_cursusregel.ToList().Where(b => b.Overlap(regel) == false && b.zs_cursus.bootgroep == zs.zs_cursus.Where(c => c.id == cid).Select(c => c.bootgroep).Sum()))
                    {
                        foreach (var boot in item.zs_bootlinktabel)
                        {
                            if (!templist.Contains(boot.zs_boten))
                            {
                                templist.Add(boot.zs_boten);
                            }
                        }
                    }
                    foreach (var boot in zs.zs_boten.Where(b => b.zs_bootlinktabel.Count() == 0 && b.bootgroep == zs.zs_cursus.Where(c => c.id == cid).Select(c => c.bootgroep).Sum()))
                    {
                        if (!templist.Contains(boot))
                        {
                            templist.Add(boot);
                        }
                    }

                    //haal boten die in onderhoud zijn er uit
                    var onderhoud = zs.zs_onderhoud.Where(o => (
                         (regel.begintijdendatum <= o.beginonderhoud && regel.eindtijdendatum >= o.beginonderhoud) ||
                         (regel.begintijdendatum >= o.beginonderhoud && regel.begintijdendatum <= o.eindonderhoud) ||
                         (regel.begintijdendatum <= o.eindonderhoud && regel.eindtijdendatum >= o.eindonderhoud)
                         ) &&
                         o.zs_boten.bootgroep == zs.zs_cursus.Where(c => c.id == cid).Select(c => c.bootgroep).Sum()
                      ).ToList();

                    foreach (var boot in onderhoud)
                    {
                        zs_boten bootje = zs.zs_boten.FirstOrDefault(b => b.id == boot.vloot_id);
                        if (templist.Contains(bootje))
                        {
                            templist.Remove(bootje);
                        }
                    }                    
                    
                    //kijk hoeveel plek mogelijk is
                    int plekken = 0;
                    foreach (var b in templist)
                    {
                        plekken += b.aantalplaatsen;
                    }
                    if (plekken < zs.zs_cursus.Where(c => c.id == cid).Sum(c => c.aantalplaatsenbeschikbaar))
                    {
                        MessageBox.Show("Niet genoeg plaatsen beschikbaar");
                        return;
                    }
                    plekken = 0;
                    //voeg mogelijke boten toe
                    foreach (var b in templist)
                    {
                        if (plekken >= zs.zs_cursus.Where(c => c.id == cid).Sum(c => c.aantalplaatsenbeschikbaar)) { break; }
                        zs_bootlinktabel blt = new zs_bootlinktabel();
                        blt.botenid = b.id;
                        regel.zs_bootlinktabel.Add(blt);
                        plekken += b.aantalplaatsen;
                    }
                    #endregion

                    #region
                    List<zs_users> medewerkerlist = new List<zs_users>();

                    //kijk hoeveel instructeurs mogelijk zijn
                    foreach (var item in zs.zs_cursusregel.ToList().Where(b => b.Overlap(regel) == false))
                    {
                        foreach (var instructeur in item.zs_geplandeinstructeurs)
                        {
                            if (instructeur.zs_users.zs_groepen.isinstructeur == true)
                            {
                                medewerkerlist.Add(instructeur.zs_users);
                            }
                        }
                    }
                    foreach (var instructeur in zs.zs_users.Where(b => b.zs_geplandeinstructeurs.Count() == 0
                                                                    && b.zs_groepen.isinstructeur == true))
                    {
                        medewerkerlist.Add(instructeur);
                    }

                    //zijn er genoeg instructeurs?
                    if (medewerkerlist.Count() < regel.zs_bootlinktabel.Count())
                    {
                        MessageBox.Show("Niet genoeg instructeurs beschikbaar");
                        return;
                    }

                    //voeg mogelijke instructeurs toe
                    int instructeurs = 0;
                    foreach (var i in medewerkerlist)
                    {
                        if (instructeurs >= regel.zs_bootlinktabel.Count()) { break; }
                        zs_geplandeinstructeurs ilt = new zs_geplandeinstructeurs();
                        ilt.user_id = i.id;
                        regel.zs_geplandeinstructeurs.Add(ilt);
                        instructeurs++;
                    }
                    #endregion

                    zs.zs_cursusregel.Add(regel);
                    zs.SaveChanges();
                    this.Close();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                    return;
                }
            }
        }


        private void NumericInputCheck(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsNumeric((TextBox)sender, e);
        }

        private void datechanged(object sender, SelectionChangedEventArgs e)
        {
            einddatum.DisplayDateStart = begindatum.SelectedDate.Value;
            if (begindatum.SelectedDate.Value != null && einddatum.SelectedDate.Value != null)
            {
                if (begindatum.SelectedDate.Value > einddatum.SelectedDate.Value)
                {
                    einddatum.SelectedDate = begindatum.SelectedDate.Value;
                }
            }
        }
    }
}
