﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Onderhouddatum.xaml
    /// </summary>
    public partial class Onderhouddatum : Window
    {
        public DateTime date1 { get; set; }
        public DateTime date2 { get; set; }
        public Onderhouddatum()
        {
            InitializeComponent();

            Begindatum.SelectedDate = DateTime.Now;
            Einddatum.SelectedDate = DateTime.Now;

            Begindatum.DisplayDateStart = DateTime.Today;
            Einddatum.DisplayDateStart = DateTime.Today;
        }

        public Onderhouddatum(DateTime begindatum, DateTime einddatum)
        {
            InitializeComponent();

            Begindatum.SelectedDate = begindatum.Date;
            Einddatum.SelectedDate = einddatum.Date;

            Begindatum.DisplayDateStart = DateTime.Today;
            Einddatum.DisplayDateStart = DateTime.Today;

            beginuren.Text = begindatum.Hour.ToString();
            beginminuten.Text = begindatum.Minute.ToString();
            einduren.Text = einddatum.Hour.ToString();
            eindminuten.Text = einddatum.Minute.ToString();
        }

        private void Gaverderbtn(object sender, RoutedEventArgs e)
        {
            
            if (validateTime() == false) { MessageBox.Show("Niet alle tijden zijn ingevuld"); return; }
            
            
            date1 = Begindatum.SelectedDate.Value;
            date2 = Einddatum.SelectedDate.Value;

            TimeSpan t1 = new TimeSpan(Convert.ToInt32(beginuren.Text), Convert.ToInt32(beginminuten.Text), 0);
            date1 = date1.Date + t1;

            TimeSpan t2 = new TimeSpan(Convert.ToInt32(einduren.Text), Convert.ToInt32(eindminuten.Text), 0);
            date2 = date2.Date + t2;

           

            this.DialogResult = true;
            this.Close();
        }

        private void Annulerenbtn(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void numbers(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsNumeric((TextBox)sender, e);
        }

        private Boolean validateTime()
        {
            if (
                string.IsNullOrWhiteSpace(beginuren.Text) ||
                string.IsNullOrWhiteSpace(beginminuten.Text) ||
                string.IsNullOrWhiteSpace(einduren.Text) ||
                string.IsNullOrWhiteSpace(eindminuten.Text)
                ) { return false; }
            if (Convert.ToInt32(beginuren.Text) < 0 || Convert.ToInt32(beginuren.Text) > 23) { return false; }
            if (Convert.ToInt32(einduren.Text) < 0 || Convert.ToInt32(einduren.Text) > 23) { return false; }

            if (Convert.ToInt32(beginminuten.Text) < 0 || Convert.ToInt32(beginminuten.Text) >59) { return false; }
            if (Convert.ToInt32(eindminuten.Text) < 0 || Convert.ToInt32(eindminuten.Text) > 59) { return false; }
            
            return true;
        }

    }
}
