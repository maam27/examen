﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for VerwerkAanmelding.xaml
    /// </summary>
    public partial class VerwerkAanmelding : Window
    {
        private int inschrijfid { get; set; }
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();
        public VerwerkAanmelding()
        {
            InitializeComponent();
            Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
            this.Close();
        }
        public VerwerkAanmelding(int AanmeldingId)
        {
            InitializeComponent();
            laadinschrijvingGegevens(AanmeldingId);
            inschrijfid = AanmeldingId;
        }

        private void laadinschrijvingGegevens(int id)
        {
            var inschrijfgegevens = zs.zs_inschrijvingen.FirstOrDefault(g => g.id == id);

            labelCursistnaam.Content = string.Format("Cursist: {0} {1}, {2}", inschrijfgegevens.zs_users.tussenvoegsel, inschrijfgegevens.zs_users.achternaam, inschrijfgegevens.zs_users.voornaam);
            labelCursusmobiel.Content = string.Format("Mobiele nummer: {0}", inschrijfgegevens.zs_users.mobnummer);
            labelCursuspostcode.Content = string.Format("Postcode: {1}{0}", inschrijfgegevens.zs_users.postcode, inschrijfgegevens.zs_users.postcode_cijfer);
            labelCursusWoonplaats.Content = string.Format("Woonplaats: {0}", inschrijfgegevens.zs_users.plaats);
            labelCursusstraat.Content = string.Format("Adres: {0} {1}", inschrijfgegevens.zs_users.straat, inschrijfgegevens.zs_users.straatnr);
            labelCursustelefoon.Content = string.Format("Telefoon nr. {0}", inschrijfgegevens.zs_users.telnummer);
            labelCursusdiploma.Content = string.Format("Diploma: {0}", inschrijfgegevens.zs_users.zwemdiploma);

            labelCursusnaam.Content = string.Format("Cursus: {0}", inschrijfgegevens.zs_cursusregel.zs_cursus.naam);
            labelCursusbeschrijving.Text = string.Format("Beschrijving: {0}", inschrijfgegevens.zs_cursusregel.zs_cursus.beschrijving);
            labelCursustijden.Content = string.Format("Begint op {0:dd-MM-yyyy} om {0:hh:mm} en eindigt op {1:dd-MM-yyyy} om {1:hh:mm}",
               inschrijfgegevens.zs_cursusregel.begintijdendatum,
                inschrijfgegevens.zs_cursusregel.eindtijdendatum
            );

            var plaatsenbezet = zs.zs_inschrijvingen.Count(c => c.zs_cursusregel.id == inschrijfgegevens.zs_cursusregel.id && c.status == "Geaccepteerd");
            int plaatsenbeschikbaar = inschrijfgegevens.zs_cursusregel.zs_cursus.aantalplaatsenbeschikbaar;

            labelCursusaantalplaatsen.Content = string.Format("Plaatsen bezet {0}/{1}", plaatsenbezet, plaatsenbeschikbaar);
            if (plaatsenbezet >= plaatsenbeschikbaar) { labelCursusaantalplaatsen.Foreground = Brushes.Red; }
        }

        private void AanmeldingGoedkeuren(object sender, RoutedEventArgs e)
        {
            zs_inschrijvingen inschrijving = zs.zs_inschrijvingen.FirstOrDefault(g => g.id == inschrijfid);
            inschrijving.status = "Geaccepteerd";
            Random rand = new Random(4);
            inschrijving.factuurnr = Convert.ToInt32(String.Format("{0}{1:000000}", inschrijving.id, rand.Next(0, 999999)));
            inschrijving.datumchanged = DateTime.Now.Date.ToShortDateString();

            var gegevens = zs.zs_inschrijvingen.FirstOrDefault(g => g.id == inschrijfid);

            string geslacht = "";
            if (gegevens.zs_users.geslacht == "m")
            { geslacht = "heer"; }
            else
            { geslacht = "mevrouw"; }
            #region mail
            Mail.SendTo(
                gegevens.zs_users.email,
                string.Format("Inschrijving '{0}'", gegevens.zs_cursusregel.zs_cursus.naam),
@"
<html>

<style>
body {background-color:lightgrey;}
.Page{
width:750px;
height:auto;
}
.Top {
background-color:#0096D9;
width:750px;
height:78px;
margin:0px;
}
.Middle {
background-color:white;
width:745px;
padding-left:5px;
padding-bottom:5px;
padding-top:2px;
height:auto;
margin:0px;}
.Bottom {
background-color:#0096D9;
width:750px;
height:78px;
}
.plaatje {
margin-left:550px;
}
.bottomtext {
margin-left:550px;
}
</style>
" + String.Format(@"
<div class='Page'>
	<div class='Top'>
			<img src='http://10.9.115.22/images/LogoSmall.png' />
	</div>
	<div class='Middle'>
		<h1>Beste, {1}</h1>
		<p>Bij deze willen wij u informeren dat uw inschrijving voor de cursus {2} is geaccepteerd.</p>
		<p>De cursus zal beginnen op {3:dd-MM-yyyy} om {3:hh:mm}.</p>
        <p>De factuur kunt u <a href='http://10.9.115.22/include/plugins/fpdf/factuur.php?id={4}' target='_new'>hier</a> vinden.<p/>
		<p>Wij hopen u dan te zien.</p>
        <p>Met vriendelijke groet,</p>
        <p>Zeilschool de Waai</p>
	<p class='bottomtext'>		
		De Waai<br>
		NanneWiid 12<br>
		8314 PM Earnewoude<br>
		05134 56 78 34<br>
		DeWaai@hotmail.com
	</p>
	</div>	
	<div class='Bottom'>
		<div class='plaatje'>
			<img  src='http://10.9.115.22/images/LogoSmall.png' />
		</div>
	</div>
</div>
</html>
",
         geslacht,
         gegevens.zs_users.voornaam + " " + gegevens.zs_users.tussenvoegsel + " " + gegevens.zs_users.achternaam,
         gegevens.zs_cursusregel.zs_cursus.naam,
        gegevens.zs_cursusregel.begintijdendatum,
         gegevens.factuurnr
         ));
            #endregion mail
            zs.SaveChanges();
            this.Close();
        }

        private void AanmeldingAfwijzen(object sender, RoutedEventArgs e)
        {
            var screen = new AfmeldingPopup();
            screen.Owner = this;
            if (screen.ShowDialog() == true)
            {
                string reden = screen.redentext;

                zs_inschrijvingen inschrijving = zs.zs_inschrijvingen.FirstOrDefault(g => g.id == inschrijfid);
                inschrijving.status = "Afgewezen";
                var gegevens = zs.zs_inschrijvingen.FirstOrDefault(g => g.id == inschrijfid);

                Mail.SendTo(
                    gegevens.zs_users.email,
                    string.Format("Inschrijving '{0}'", gegevens.zs_cursusregel.zs_cursus.naam),
    @"
<html>
<style>
body {background-color:lightgrey;}
.Page{
width:750px;
height:auto;
}
.Top {
background-color:#0096D9;
width:750px;
height:78px;
margin:0px;
}
.Middle {
background-color:white;
width:745px;
padding-left:5px;
padding-bottom:5px;
padding-top:2px;
height:auto;
margin:0px;}
.Bottom {
background-color:#0096D9;
width:750px;
height:78px;
}
.plaatje {
margin-left:550px;
}
.bottomtext {
margin-left:550px;
}
</style>
" + String.Format(@"
<div class='Page'>
	<div class='Top'>
			<img src='http://10.9.115.22/images/LogoSmall.png' />
	</div>
	<div class='Middle'>
		<h1>Beste, {1}</h1>
		<p>Bij deze willen wij u informeren dat uw inschrijving voor de cursus {2} is afgewezen.</p>
        <p>{0}</p>
        <p>Met vriendelijke groet,</p>
        <p>Zeilschool de Waai</p>
	<p class='bottomtext'>		
		De Waai<br>
		NanneWiid 12<br>
		8314 PM Earnewoude<br>
		05134 56 78 34<br>
		DeWaai@hotmail.com
	</p>
	</div>	
	<div class='Bottom'>
		<div class='plaatje'>
			<img  src='http://10.9.115.22/images/LogoSmall.png' />
		</div>
	</div>
</div>
</html>
",
             reden,
        gegevens.zs_users.voornaam + " " + gegevens.zs_users.tussenvoegsel + " " + gegevens.zs_users.achternaam,
        gegevens.zs_cursusregel.zs_cursus.naam,
           gegevens.zs_cursusregel.begintijdendatum
             ));

                zs.SaveChanges();
                this.Close();
            }
        }
    }
}
