﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /**
     * Written by Theproudwolf Aka Rick van Druten
     * Klanten form
     * */

    /// <summary>
    /// Interaction logic for KlantenBeheer.xaml
    /// </summary>
    public partial class KlantenBeheer : Window
    {

        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        public KlantenBeheer()
        {
            InitializeComponent();
            User.FillUserList(KlantenList);
        }

        private void KlantenList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FillData();
        }


        private void LoadFactuurs()
        {
            factuurtxt.Items.Clear();

            var facturen = new GridView();
            this.factuurtxt.View = facturen;
            facturen.Columns.Add(new GridViewColumn { Header = "Factuur nummer", DisplayMemberBinding = new Binding("factuurnr") });
            facturen.Columns.Add(new GridViewColumn { Header = "Betaald", DisplayMemberBinding = new Binding("betaald") });
            facturen.Columns.Add(new GridViewColumn { Header = "Datum ingeschreven", DisplayMemberBinding = new Binding("datumaangevraagd") });
            facturen.Columns.Add(new GridViewColumn { Header = "Datum factuur", DisplayMemberBinding = new Binding("datumchanged") });

            zs_users currentuser = KlantenList.SelectedItem as zs_users;
            if (currentuser != null)
            {
                        foreach ( zs_inschrijvingen factuurtje in zs.zs_inschrijvingen)
                        {
                            if (factuurtje.users_id == currentuser.id && factuurtje.status == "Geaccepteerd")
                            {
                                    factuurtxt.Items.Add(factuurtje);
                            }
                        }
            }
        }


        private void saveBTN(object sender, RoutedEventArgs e)
        {
                 zs_users currentuser = KlantenList.SelectedItem as zs_users;
                 zs_users current = zs.zs_users.SingleOrDefault(i => i.id == currentuser.id);

                 zs_groepen currentgroep = grouptxt.SelectedItem as zs_groepen;
                 zs_groepen currentg = zs.zs_groepen.SingleOrDefault(i => i.id == currentgroep.id);
                 if (current != null)
                 {
                     current.zs_groepen = currentg;
                     zs.SaveChanges();
                     //Refresh de list.
                     User.FillUserList(KlantenList);
                     FillData();
                     MessageBox.Show("Gegevens opgeslagen");
                 }
        }

        private void factuurtxt_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            zs_inschrijvingen Factuur = factuurtxt.SelectedItem as zs_inschrijvingen;
            if (Factuur != null)
            {
                bekijkfactuurtje.Visibility = Visibility.Visible;

                if (Factuur.betaald == 0)
                {
                    heeftbetaald.Visibility = Visibility.Visible;
                }
                else
                {
                    heeftbetaald.Visibility = Visibility.Hidden;
                }

            }
            else
            {
                bekijkfactuurtje.Visibility = Visibility.Hidden;
                heeftbetaald.Visibility = Visibility.Hidden;
            }
        }

        private void bekijkfactuurBTN(object sender, RoutedEventArgs e)
        {
            zs_inschrijvingen Factuur = factuurtxt.SelectedItem as zs_inschrijvingen;
            System.Diagnostics.Process.Start("http://10.9.115.22/include/plugins/fpdf/factuur.php?id="+Factuur.factuurnr);
        }

        private void heeftbetaaldBTN(object sender, RoutedEventArgs e)
        {
            zs_inschrijvingen Factuur = factuurtxt.SelectedItem as zs_inschrijvingen;
            Factuur.betaald = 1;
            zs.SaveChanges();
            LoadFactuurs();
            MessageBox.Show("De factuur is gewijzigd naar betaald.");
        }

        private void FillData()
        {
            zs_users currentuser = KlantenList.SelectedItem as zs_users;
            if (currentuser != null)
            {
                idtxt.Text = currentuser.id.ToString();
                emailtxt.Text = currentuser.email;
                naamtxt.Text = User.GetUserFullName(currentuser);
                geslachttxt.Text = User.GetGeslacht(currentuser);
                plaatstxt.Text = currentuser.plaats;
                adrestxt.Text = currentuser.straat + " " + currentuser.straatnr;
                postcodetxt.Text = currentuser.postcode_cijfer + " " + currentuser.postcode;
                telefoontxt.Text = currentuser.telnummer.ToString();
                mobielnummertxt.Text = currentuser.mobnummer.ToString();
                geboortedatumtxt.Text = currentuser.geboortedatum.ToShortDateString();

                //Het vullen van de combobox
                Group.FillComboBox(grouptxt, currentuser);
                savebutton.Visibility = Visibility.Visible;
                LoadFactuurs();

            }
        }

     

    }
}
