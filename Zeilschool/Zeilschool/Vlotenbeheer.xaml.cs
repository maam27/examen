﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for Vlotenbeheer.xaml
    /// </summary>
    public partial class Vlotenbeheer : Window
    {
        zeilschoolnewEntities1 zs = new zeilschoolnewEntities1();

        private Boolean edit { get; set; }
        private String[] Afbeeldingen { get; set; }

        private int LastSelectedId { get; set; }
        public Vlotenbeheer()
        {
            InitializeComponent();

            Fillgroupbox();
            FillFleetList();

            this.VlootList.SelectionChanged += VlootList_SelectionChanged;
        }

        //vloot lijst vullen
        public void FillFleetList()
        {
            var test =
                zs.zs_boten.Select(i => new
                {
                    Id = i.id,
                    Naam = i.naam,
                    Boot_Type = i.type
                });

            try
            {
                VlootList.ItemsSource = test.ToList();
            }
            catch (Exception err) { MessageBox.Show(err.Message); }
        }

        //laat gegevens van geselecteerde vloot zien
        private void VlootList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (VlootList.SelectedIndex == -1 || edit == true) { return; }
            Fillgroupbox();

            zs_boten SelectedVloot = GetSelectedBoat();


            LastSelectedId = SelectedVloot.id;
            VlootNaamTxt.Text = SelectedVloot.naam;
            VlootBeschrijvingTxt.Text = SelectedVloot.beschrijving;
            VlootPlaatsenTxt.Text = SelectedVloot.aantalplaatsen.ToString();
            groepbox.SelectedItem = SelectedVloot.zs_bootgroeplink.groepnaam;

            var img = zs.zs_bootafbeeldingen.FirstOrDefault(i => i.boot_id == SelectedVloot.id);
            if (img != null)
                VlootImgPath.SelectedItem = System.IO.Path.GetFileName(img.afbeelding);
            VlootType.Text = SelectedVloot.type;
        }

        private zs_boten GetSelectedBoat()
        {
            object item = VlootList.SelectedItem;
            int ID = Convert.ToInt32((VlootList.SelectedCells[0].Column.GetCellContent(item) as TextBlock).Text);
            zs_boten boot = zs.zs_boten.FirstOrDefault(b => b.id == ID);

            return boot;
        }

        private void Fillgroupbox()
        {
            groepbox.Items.Clear();
            var groeps = zs.zs_bootgroeplink.Select(g => g.groepnaam).Distinct();
            foreach (var groepnaam in groeps)
            {
                groepbox.Items.Add(groepnaam);
            }
            VlootImgPath.Items.Clear();
            var images = FTP.GetFilesInFtpDirectory("ftp://10.9.115.22/images/upload/boot/", "imageupload", "LOL");
            foreach (var img in images)
            {
                this.VlootImgPath.Items.Add(img);
            }
        }

        private void NewVloot(object sender, RoutedEventArgs e)
        {
            FillFleetList();
            Controls.ClearAllTextBoxes(this);
            Controls.UnlockAllTextboxes(this);
            HideButtons();
            BtnSave.Visibility = Visibility.Visible;
            BtnCancel.Visibility = Visibility.Visible;
        }

        private void HideButtons()
        {
            BtnNew.Visibility = Visibility.Hidden;
            BtnEdit.Visibility = Visibility.Hidden;
            BtnSave.Visibility = Visibility.Hidden;
            BtnSaveChange.Visibility = Visibility.Hidden;
            BtnRemove.Visibility = Visibility.Hidden;
            BtnCancel.Visibility = Visibility.Hidden;
        }
        //vloot edit oplaan
        private void EditVloot(object sender, RoutedEventArgs e)
        {
            if (VlootList.SelectedIndex == -1) { return; }
            edit = true;

            Controls.UnlockAllTextboxes(this);
            HideButtons();

            BtnSaveChange.Visibility = Visibility.Visible;
            BtnCancel.Visibility = Visibility.Visible;
        }
        //verwijder vloot
        private void RemoveVloot(object sender, RoutedEventArgs e)
        {
            if (VlootList.SelectedIndex == -1) { return; }
            zs_boten selectedItem = GetSelectedBoat();
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Weet je zeker dat u deze vloot wilt verwijderen?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                zs_boten C = zs.zs_boten.Where(Vloot => Vloot.id == selectedItem.id).FirstOrDefault();
                zs_bootafbeeldingen img = zs.zs_bootafbeeldingen.FirstOrDefault(i => i.boot_id == C.id);
                try { zs.zs_bootafbeeldingen.Remove(img); }
                catch (Exception err) { }
                zs.zs_boten.Remove(C);
                zs.SaveChanges();
                FillFleetList();
            }
        }

        //sla nieuwe boot op
        private void SaveVloot(object sender, RoutedEventArgs e)
        {
            if (!IsValidated()) { return; }

            zs_bootgroeplink SelectedGroep = zs.zs_bootgroeplink.FirstOrDefault(g => g.groepnaam == groepbox.SelectedValue);
            zs_boten SelectedVloot = VlootList.SelectedItem as zs_boten;
            zs_boten NewVloot = new zs_boten
            {
                beschrijving = VlootBeschrijvingTxt.Text,
                naam = VlootNaamTxt.Text,
                aantalplaatsen = Convert.ToInt32(VlootPlaatsenTxt.Text),
                bootgroep = SelectedGroep.id,
                type = VlootType.Text,
            };

            foreach (var img in Afbeeldingen)
            {
                zs_bootafbeeldingen afbeelding = new zs_bootafbeeldingen();
                afbeelding.volgorde = 1;
                afbeelding.boot_id = NewVloot.id;
                afbeelding.afbeelding = String.Format("{0}{1}", "http://10.9.115.22/images/upload/boot/", System.IO.Path.GetFileName(img));
                zs.zs_bootafbeeldingen.Add(afbeelding);
                VlootImgPath.SelectedValue = System.IO.Path.GetFileName(img);
            }
            zs.zs_boten.Add(NewVloot);
            try
            {
                var ftpimages = FTP.GetFilesInFtpDirectory("ftp://10.9.115.22/images/upload/boot/", "imageupload", "LOL");
                foreach (var i in Afbeeldingen)
                {
                    if (ftpimages.Contains(System.IO.Path.GetFileName(i)))
                    {
                        MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("De afbeelding die u heeft geselecteerd bestaat al, wilt u deze overschrijven?", "Afbeelding uploaden", System.Windows.MessageBoxButton.YesNo);
                        if (messageBoxResult == MessageBoxResult.Yes)
                        {
                            FTP.UploadImage("boot/", Afbeeldingen);
                        }
                    }
                    else
                    {
                        FTP.UploadImage("boot/", Afbeeldingen);
                    }
                }

                zs.SaveChanges();
            }
            catch (Exception error)
            {
                MessageBox.Show("Er is iets fout gegaan tijdens het opslaan.");
            }
            FillFleetList();

            Controls.LockAllTextboxes(this);
            HideButtons();
            BtnNew.Visibility = Visibility.Visible;
            BtnEdit.Visibility = Visibility.Visible;
            BtnRemove.Visibility = Visibility.Visible;
            edit = false;
        }

        private Boolean IsValidated()
        {
            String ErrorMsg = "";

            if (string.IsNullOrWhiteSpace(VlootNaamTxt.Text)) { ErrorMsg += "Vloot naam mag niet leeg zijn." + Environment.NewLine; }
            if (string.IsNullOrWhiteSpace(VlootBeschrijvingTxt.Text)) { ErrorMsg += "Vloot beschrijving mag niet leeg zijn." + Environment.NewLine; }
            if (Convert.ToInt32(VlootPlaatsenTxt.Text) <= 0) { ErrorMsg += "Vloot mag niet minder dan 1 plek hebben." + Environment.NewLine; }
            if (string.IsNullOrWhiteSpace(VlootType.Text)) { ErrorMsg += "Vloot type mag niet leeg zijn." + Environment.NewLine; }
            if (Afbeeldingen == null) { ErrorMsg += "U heeft geen afbeelding opgegeven." + Environment.NewLine; }
            else
            {
                foreach (var img in Afbeeldingen)
                {
                    if (String.IsNullOrEmpty(img)) { ErrorMsg += "U heeft geen afbeelding opgegeven." + Environment.NewLine; break; }
                }
            }

            //show error message
            if (!string.IsNullOrWhiteSpace(ErrorMsg)) { MessageBox.Show("Foutmelding bericht" + Environment.NewLine + ErrorMsg); return false; }
            return true;
        }

        private void SaveVlootChanges(object sender, RoutedEventArgs e)
        {
            if (!IsValidated()) { return; }
            HideButtons();
            Controls.LockAllTextboxes(this);
            BtnNew.Visibility = Visibility.Visible;
            BtnEdit.Visibility = Visibility.Visible;
            BtnRemove.Visibility = Visibility.Visible;

            zs_boten SelectedVloot = GetSelectedBoat();
            edit = false;

            //update huidige data
            zs_bootgroeplink SelectedGroep = zs.zs_bootgroeplink.FirstOrDefault(g => g.groepnaam == groepbox.SelectedValue);

            SelectedVloot.naam = VlootNaamTxt.Text;
            SelectedVloot.beschrijving = VlootBeschrijvingTxt.Text;
            SelectedVloot.aantalplaatsen = Convert.ToInt32(VlootPlaatsenTxt.Text);
            SelectedVloot.bootgroep = SelectedGroep.id;
            SelectedVloot.type = VlootType.Text;
            zs.SaveChanges();

            foreach (var img in Afbeeldingen)
            {
                zs_bootafbeeldingen bootafbeelding = zs.zs_bootafbeeldingen.Where(ba => ba.boot_id == SelectedVloot.id).FirstOrDefault();
                if (bootafbeelding != null)
                {
                    bootafbeelding.afbeelding = String.Format("{0}{1}", "http://10.9.115.22/images/upload/boot/", System.IO.Path.GetFileName(img));
                }
                else 
                {
                    zs_bootafbeeldingen afbeelding = new zs_bootafbeeldingen();
                    afbeelding.boot_id = SelectedVloot.id;
                    afbeelding.afbeelding = String.Format("{0}{1}", "http://10.9.115.22/images/upload/boot/", System.IO.Path.GetFileName(img));
                    zs.zs_bootafbeeldingen.Add(afbeelding);
                }
                    zs.SaveChanges();
                    VlootImgPath.SelectedValue = System.IO.Path.GetFileName(img);
                
            }

            FillFleetList();
        }

        private void CancelVloot(object sender, RoutedEventArgs e)
        {
            HideButtons();
            Controls.LockAllTextboxes(this);
            BtnNew.Visibility = Visibility.Visible;
            BtnEdit.Visibility = Visibility.Visible;
            BtnRemove.Visibility = Visibility.Visible;
            edit = false;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Nummers(object sender, TextCompositionEventArgs e)
        {
            Controls.CheckIsNumeric((TextBox)sender, e);
        }

        private void SearchImage(object sender, RoutedEventArgs e)
        {
            Afbeeldingen = FTP.SelectFiles();
        }

        private void VlootImgPath_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Afbeeldingen = new[] { VlootImgPath.SelectedItem.ToString() };
            }
            catch (Exception errrrrr) { Afbeeldingen = new[] { "" }; }
        }
    }
}
