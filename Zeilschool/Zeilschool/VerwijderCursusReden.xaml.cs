﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zeilschool
{
    /// <summary>
    /// Interaction logic for VerwijderCursusReden.xaml
    /// </summary>
    public partial class VerwijderCursusReden : Window
    {
        public string reden { get; set; }
        public VerwijderCursusReden()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            reden = redentxt.Text;
            this.DialogResult = true;
            this.Close();
        }
    }
}
